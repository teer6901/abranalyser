function updater
    
    [status, result] = system('git --version');
    
    if status ~= 0 || ~contains(result, 'git version')
        error(sprintf(['git version control not found on this computer\n' ...
                       'Please install from\n' ...
                       '<a href="https://git-scm.com/download">https://git-scm.com/download</a>\n\n']));
    end
    
    if ~exist('.git', 'dir')
        fprintf('============================================================\n');
        fprintf('    git version control will be initialized...\n');
        fprintf('\n');
        syscall('git init');
        syscall('git remote add origin https://gitlab.uni-oldenburg.de/teer6901/abranalyser.git');
        syscall('git fetch origin release');
        syscall('git checkout -f release');
        syscall('git checkout -B local');
        fprintf('\n');
        fprintf('    git version control is ready to be used.\n');
        fprintf('============================================================\n');
    else
        fprintf('============================================================\n');
        fprintf('    updating git version control\n');
        fprintf('\n');
        syscall('git checkout release');
        syscall('git pull origin release');
        syscall('git checkout local');
        syscall('git merge release');
        fprintf('\n');
        fprintf('============================================================\n');
    end
end

function syscall(command)
    [status, result]  = system(command);
    if status == 0
        fprintf('%s\n', result);
    else
        error('git_updater:system_error', '%s\n', result);
    end
end
