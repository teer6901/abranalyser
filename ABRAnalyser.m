function varargout = ABRAnalyser(varargin)
    % ABRANALYSER MATLAB code for ABRAnalyser.fig
    %      ABRANALYSER, by itself, creates a new ABRANALYSER or raises the existing
    %      singleton*.
    %
    %      H = ABRANALYSER returns the handle to a new ABRANALYSER or the handle to
    %      the existing singleton*.
    %
    %      ABRANALYSER('CALLBACK',hObject,eventData,handles,...) calls the local
    %      function named CALLBACK in ABRANALYSER.M with the given input arguments.
    %
    %      ABRANALYSER('Property','Value',...) creates a new ABRANALYSER or raises the
    %      existing singleton*.  Starting from the left, property value pairs are
    %      applied to the GUI before ABRAnalyser_OpeningFcn gets called.  An
    %      unrecognized property name or invalid value makes property application
    %      stop.  All inputs are passed to ABRAnalyser_OpeningFcn via varargin.
    %
    %      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
    %      instance to run (singleton)".
    %
    % See also: GUIDE, GUIDATA, GUIHANDLES
    
    % Edit the above text to modify the response to help ABRAnalyser
    
    % Last Modified by GUIDE v2.5 01-Sep-2021 10:30:36
    
    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
        'gui_Singleton',  gui_Singleton, ...
        'gui_OpeningFcn', @ABRAnalyser_OpeningFcn, ...
        'gui_OutputFcn',  @ABRAnalyser_OutputFcn, ...
        'gui_LayoutFcn',  [] , ...
        'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end
    
    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code - DO NOT EDIT
    
    
    % --- Executes just before ABRAnalyser is made visible.
function ABRAnalyser_OpeningFcn(hObject, eventdata, handles, varargin)
    % This function has no output args, see OutputFcn.
    % hObject    handle to figure
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    % varargin   command line arguments to ABRAnalyser (see VARARGIN)
    
    handles.CurrentPath = '.';
    
    handles.Settings.FilterCutOff = [300 3000]; %[50 3000];
    handles.Settings.FIRFilterOrder  = 512; %100;
    handles.Settings.IIRFilterOrder  = 4;
    handles.Settings.FilterType = 'FIR';
    % handles.Settings.FilterType = 'none';
    handles.Settings.TimeLimits = [-3 9];
    
    handles.StimType = '';
    handles.StimFreq = 0;
    
    % Choose default command line output for ABRAnalyser
    handles.output = hObject;
    
    handles.CurrentFile = '';
    
    handles.CurrentRow = [];
    
    handles.NumWaves = 4;
    handles.Candidates.Lat = zeros(handles.NumWaves,1);
    handles.Candidates.Amp = zeros(handles.NumWaves,1);
    handles.Candidates.Idx = 1:handles.NumWaves;
    
    handles.Candidates.BDPLat = zeros(handles.NumWaves,1);
    handles.Candidates.BDPAmp = zeros(handles.NumWaves,1);
    handles.Candidates.BDPIdx = 1;
    
    handles.NumDPs = 2;
    handles.Candidates.DPLat = zeros(10,1);
    handles.Candidates.DPAmp = zeros(10,1);
    handles.Candidates.DPIdx = 1:handles.NumDPs;
    
    handles.IsSwitched = false;
    
    handles.Table     = {[],[]};
    handles.LastIndex = 0;
    
    handles.CursorMode = false;
    
    handles.Settings = getSavedSettings(handles.Settings);
    
    rectangle('position',[0 -50 1 100],'edgecolor','none','facecolor',[1   0.8 1  ])
    rectangle('position',[1 -50 1 100],'edgecolor','none','facecolor',[0.8 1   0.8])
    rectangle('position',[2 -50 1 100],'edgecolor','none','facecolor',[1   1   0.8])
    rectangle('position',[3 -50 1 100],'edgecolor','none','facecolor',[0.8 0.8 1  ])
    rectangle('position',[4 -50 1 100],'edgecolor','none','facecolor',[1   0.8 0.8])
    rectangle('position',[5 -50 1 100],'edgecolor','none','facecolor',[0.8 1   1  ])
    set(gca,'layer','top')
    hold on
    handles.ZeroLine = plot(handles.axWaveForm,[-100;100],[0;0],'linestyle','--','color',[0.3 0.3 0.3]);
    handles.SumLine  = plot(handles.axWaveForm,0,0,'-','color',[0.65 0.65 0.65]);
    handles.BDPLine  = plot(handles.axWaveForm,0,0,'r--');
    handles.WaveLine = plot(handles.axWaveForm,0,0,'k-');
    handles.CursorLines = line([0 0;0 0],[0 0;0 0],'linestyle','-','color','k','visible','off');
    handles.CursorText  = text(0,0,'','linestyle','-','color','k','backgroundcolor','w','visible','off');
    hold off
    
    WaveText = {'I','II','III','IV'};
    for k = 1:handles.NumWaves
        handles.WaveMarker(k) = line(handles.Candidates.Lat(handles.Candidates.Idx(k)),...
            handles.Candidates.Amp(handles.Candidates.Idx(k)),...
            'marker','o','color','r','linewidth',1,'linestyle','none','markersize',8);
        handles.WaveText(k) = text(handles.Candidates.Lat(handles.Candidates.Idx(k)),...
            handles.Candidates.Amp(handles.Candidates.Idx(k))+0.1,...
            WaveText{k},'HorizontalAlignment','center','VerticalAlignment','bottom',...
            'fontsize',14,'fontweight','bold');
    end
    
    handles.BDPMarker = line(handles.Candidates.BDPLat(handles.Candidates.BDPIdx),...
        handles.Candidates.BDPAmp(handles.Candidates.BDPIdx),...
        'marker','x','color','r','linewidth',1,'linestyle','none','markersize',12);
    handles.BDPText = text(handles.Candidates.BDPLat(handles.Candidates.BDPIdx),...
        handles.Candidates.BDPAmp(handles.Candidates.BDPIdx)+0.1,...
        'BDP','HorizontalAlignment','center','VerticalAlignment','bottom',...
        'fontsize',14,'fontweight','bold');
    for k = 1:handles.NumDPs
        handles.DPMarker(k) = line(handles.Candidates.DPLat(handles.Candidates.DPIdx(k)),...
            handles.Candidates.DPAmp(handles.Candidates.DPIdx(k)),...
            'marker','^','color','r','linewidth',1,'linestyle','none','markersize',12, 'visible', 'off');
        handles.DPText(k) = text(handles.Candidates.DPLat(handles.Candidates.DPIdx(k)),...
            handles.Candidates.DPAmp(handles.Candidates.DPIdx(k))+0.1,...
            sprintf('DP%1.0f',k),'HorizontalAlignment','center','VerticalAlignment','bottom',...
            'fontsize',14,'fontweight','bold', 'visible', 'off');
    end
    
    
    handles.min_markers = feature_marker.empty;
    
    handles.PrintFig = [];
    
    % Update handles structure
    guidata(hObject, handles);
    
    
    % UIWAIT makes ABRAnalyser wait for user response (see UIRESUME)
    % uiwait(handles.figABRAnalyser);
    
    
    % --- Outputs from this function are returned to the command line.
function varargout = ABRAnalyser_OutputFcn(hObject, eventdata, handles)
    % varargout  cell array for returning output args (see VARARGOUT);
    % hObject    handle to figure
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Get default command line output from handles structure
    varargout{1} = handles.output;
    
function s = getSavedSettings(s)
    p = fileparts(mfilename('fullpath'));
    settingsfile = fullfile(p, 'abranalyser_settings.mat');
    if exist(settingsfile, 'file')
        sf = load(settingsfile);
        flds = fieldnames(sf);
        for fx = 1:length(flds)
            s.(flds{fx}) = sf.(flds{fx});
        end
    end

function saveSettings(handles)
    p = fileparts(mfilename('fullpath'));
    settingsfile = fullfile(p, 'abranalyser_settings.mat');
    s = handles.Settings;
    save(settingsfile, '-struct', 's');
    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% File menu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function mnFile_Callback(hObject, eventdata, handles)
    
    
function mnOpen_Callback(hObject, eventdata, handles)
    [FileNames,PathName] = uigetfile({'*.mat','data files';'*.m','file list'},'Select Files',[handles.CurrentPath '/'],'MultiSelect','on');
    if iscellstr(FileNames) || ischar(FileNames)
        single_data = true;
        if ~isempty(PathName)
            handles.CurrentPath = PathName;
        end
        if ~isempty(FileNames)
            handles.Table     = {[],[]};
            handles.LastIndex = 0;
            if ischar(FileNames) == 1 && strcmp(FileNames(end-1:end),'.m')
                tmppath = pwd;
                cd(PathName);
                eval(FileNames(1:end-2));
                cd(tmppath);
                single_data = false;
            elseif ischar(FileNames) == 1 && strcmp(FileNames(end-3:end),'.mat')
                tmp = who('-file',fullfile(PathName,FileNames));
                if any(strcmp(tmp,'Table')) && any(strcmp(tmp,'Files')) && any(strcmp(tmp,'Path'))
                    tmp = load(fullfile(PathName,FileNames));
                    handles.Table = tmp.Table;
                    handles.CurrentPath = tmp.Path;
                    FileNames = tmp.Files;
                    single_data = false;
                end
            end
            if single_data
                handles.Table = {[],[]};
                if iscellstr(FileNames)
                    Files = cellstr(FileNames(:));
                else
                    Files = {FileNames};
                end
                FileNames = cell(length(Files),1);
                for fx = 1:length(Files)
                    FileNames{fx} = Files(fx);
                end
            end
            if ~isempty(FileNames)
                % save extra file name cell array
                handles.FileNames = FileNames;
                % create comma separated list for listbox
                FileList = {};
                for fx = 1:length(FileNames)
                    FileList{fx,1} = sprintf('%s,',FileNames{fx}{:});
                    FileList{fx,1} = FileList{fx,1}(1:end-1);
                end
                set(handles.lbFiles,'String',FileList);
                set(handles.lbFiles,'Value',1);
            end
        end
        
        guidata(hObject,handles);
        
        lbFiles_Callback(handles.lbFiles, [], handles);
    end
    
    
function mnSave_Callback(hObject, eventdata, handles)
    if isempty(handles.CurrentFile)
        mnSaveAs_Callback(handles.mnSaveAs, eventdata, handles);
    else
        SaveFile(handles);
    end
    
function mnSaveAs_Callback(hObject, eventdata, handles)
    [FileName,PathName] = uiputfile({'*.mat';'*.xls';'*.csv'},'Save Data As...',handles.CurrentFile);
    if iscellstr(FileName) || ischar(FileName)
        answer = 'Yes';
        if exist(fullfile(PathName,FileName),'file')
            answer = questdlg(sprintf('%s already exists. Overwrite?',FileName),'File exists!','Yes','No','No');
        end
        if strcmpi(answer,'Yes')
            handles.CurrentFile = fullfile(PathName,FileName);
            SaveFile(handles);
        end
    end
    
function SaveFile(handles,type)
    if handles.LastIndex > 0
        handles.Table{handles.LastIndex,1} = get(handles.tblData,'Data');
        [handles.Table{handles.LastIndex,1}{:,1}] = deal([]);
    end
    
    if ~isempty(handles.CurrentRow) && handles.CurrentRow > 0
        handles.Table{handles.LastIndex,2}{handles.CurrentRow,1} = handles.Candidates.Lat;
        handles.Table{handles.LastIndex,2}{handles.CurrentRow,2} = handles.Candidates.Amp;
        handles.Table{handles.LastIndex,2}{handles.CurrentRow,3} = handles.Candidates.Idx;
        handles.Table{handles.LastIndex,2}{handles.CurrentRow,4} = handles.Candidates.BDPLat;
        handles.Table{handles.LastIndex,2}{handles.CurrentRow,5} = handles.Candidates.BDPAmp;
        handles.Table{handles.LastIndex,2}{handles.CurrentRow,6} = handles.Candidates.BDPIdx;
        handles.Table{handles.LastIndex,2}{handles.CurrentRow,7} = handles.Candidates.DPLat;
        handles.Table{handles.LastIndex,2}{handles.CurrentRow,8} = handles.Candidates.DPAmp;
        handles.Table{handles.LastIndex,2}{handles.CurrentRow,9} = handles.Candidates.DPIdx;
    end
    
    guidata(handles.figABRAnalyser,handles);
    
    Table = handles.Table;
    % Files = get(handles.lbFiles,'String');
    Files = handles.FileNames;
    Path  = handles.CurrentPath;
    
    if strcmp(handles.CurrentFile(end-3:end),'.mat')
        save(handles.CurrentFile,'Table','Files','Path');
    elseif any(strcmp(handles.CurrentFile(end-3:end),{'.xls','*.csv'}))
        T = cell(1,size(Table{1,1},2));
        for k = 1:size(Table,1)
            T{end+1,1} = [];
            T{end+1,1} = Files{k};
            T = cat(1,T,Table{k,1});
        end
        if strcmp(handles.CurrentFile(end-3:end),'.xls')
            xlswrite(handles.CurrentFile,T);
        elseif strcmp(handles.CurrentFile(end-3:end),'.csv')
            dlmwrite(handles.CurrentFile,T,'delimiter',';');
        end
    end
    
    
function mnQuit_Callback(hObject, eventdata, handles)
    close(handles.figABRAnalyser);
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Edit menu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function mnEdit_Callback(hObject, eventdata, handles)
    
function mnResetRow_Callback(hObject, eventdata, handles)
    if ~isempty(handles.CurrentRow) && handles.CurrentRow > 0
        TD = get(handles.tblData,'Data');
        [TD{handles.CurrentRow,6:end}] = deal([]);
        handles.Candidates.Lat = [];
        handles.Candidates.Amp = [];
        handles.Candidates.Idx = [];
        handles.Candidates.BDPLat = [];
        handles.Candidates.BDPAmp = [];
        handles.Candidates.BDPIdx = [];
        handles.Candidates.DPLat = [];
        handles.Candidates.DPAmp = [];
        handles.Candidates.DPIdx = [];
        set(handles.tblData,'Data',TD);
        tblData_CellSelectionCallback(handles.tblData, struct('Indices',handles.CurrentRow), handles);
    end
    
    % --------------------------------------------------------------------
function mnAddBinSum_Callback(hObject, eventdata, handles)
    handles = guidata(hObject);
    TD = get(handles.tblData,'Data');
    r = 1;
    while r <= size(TD,1)
        if strcmp(TD{r,4}, ' B') && (r == size(TD,1) || ~strcmp(TD{r+1,4}, 'BS'))
            TD = cat(1,TD(1:r,:),cell(1,size(TD,2)),TD(r+1:end,:));
            if r <= size(handles.Table{handles.LastIndex,2},1)
                handles.Table{handles.LastIndex,2} = cat(1,handles.Table{handles.LastIndex,2}(1:r,:),...
                    cell(1,size(handles.Table{handles.LastIndex,2},2)),...
                    handles.Table{handles.LastIndex,2}(r+1:end,:));
            end
            if handles.CurrentRow > r
                handles.CurrentRow = handles.CurrentRow + 1;
            end
            r = r + 1;
            TD{r,1} = [];
            TD{r,2} = TD{r-1,2};
            TD{r,3} = TD{r-1,3};
            TD{r,4} = 'BS';
            TD{r,5} = handles.Data.Level;
        end
        r = r + 1;
    end
    set(handles.tblData, 'Data', TD);
    tblData_CellSelectionCallback(handles.tblData, struct('Indices',handles.CurrentRow), handles);
    guidata(hObject, handles);
    
    
function mnSwitchPol_Callback(hObject, eventdata, handles)
    % switch polarity of raw data 
    handles.Data.Avg = -handles.Data.Avg;
    guidata(hObject,handles);
    mnResetRow_Callback(handles.mnResetRow, [], handles);
    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% View menu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function mnView_Callback(hObject, eventdata, handles)
    
    
function mnSummary_Callback(hObject, eventdata, handles)
    
    if isfield(handles,'PrintFig')  && ~isempty(handles.PrintFig)
        close(handles.PrintFig(ishandle(handles.PrintFig)));
    end
    
    
    TD = get(handles.tblData,'Data');
    WaveText = {'I','II','III','IV'};
    
    MaxAxPerFig = 12;
    ymaxlim = [0 0];
    hAx = [];
    
    for row = 1:size(TD,1)
        
        ypos = mod(row-1,12)+1;
        
        % waveform
        [data,time,bdpdata,sumdata] = PrepareData(handles,row);
        
        if mod(row-1,MaxAxPerFig) == 0
            handles.PrintFig = [handles.PrintFig figure];
            set(handles.PrintFig(end),'units','centimeters','position',[0.5 0.5 21 29.7])
            set(handles.PrintFig(end),'paperunits','centimeters','papertype','A4','paperposition',[0.5 0.5 20 28.7])
            guidata(hObject,handles);
            set(gcf,'units','normalized');
            clf
        end
        
        %     subplot(size(TD,1),1,row);
        ha(2) = axes('yaxislocation','right');
        ha(1) = axes('color','none');
        
        rectangle('position',[0 -50 1 100],'edgecolor','none','facecolor',[1   0.8 1  ],'parent',ha(2))
        rectangle('position',[1 -50 1 100],'edgecolor','none','facecolor',[0.8 1   0.8],'parent',ha(2))
        rectangle('position',[2 -50 1 100],'edgecolor','none','facecolor',[1   1   0.8],'parent',ha(2))
        rectangle('position',[3 -50 1 100],'edgecolor','none','facecolor',[0.8 0.8 1  ],'parent',ha(2))
        rectangle('position',[4 -50 1 100],'edgecolor','none','facecolor',[1   0.8 0.8],'parent',ha(2))
        rectangle('position',[5 -50 1 100],'edgecolor','none','facecolor',[0.8 1   1  ],'parent',ha(2))
        set(ha(1),'layer','top')
        set(ha,'nextplot','add');
        hTmpZeroLine = plot(ha(1),[-100;100],[0;0],'linestyle',':','color',[0.3 0.3 0.3]);
        hTmpSumLine  = plot(ha(1),0,0,'-','color',[0.65 0.65 0.65]);
        hTmpBDPLine  = plot(ha(2),0,0,'r--');
        hTmpWaveLine = plot(ha(1),0,0,'k-');
        set(ha,'nextplot','replace');
        
        
        %     Lat = handles.Table{handles.LastIndex,2}{row,1};
        %     Amp = handles.Table{handles.LastIndex,2}{row,2};
        %     Idx = handles.Table{handles.LastIndex,2}{row,3};
        %     BDPLat = handles.Table{handles.LastIndex,2}{row,4};
        %     BDPAmp = handles.Table{handles.LastIndex,2}{row,5};
        %     BDPIdx = handles.Table{handles.LastIndex,2}{row,6};
        
        Lat = [TD{row,8:2:14}];
        Amp = [TD{row,9:2:15}];
        Idx = 1:min(length(Lat),length(Amp));
        BDPLat = TD{row,6};
        BDPAmp = TD{row,7};
        BDPIdx = 1;
        DPLat = [TD{row,16:2:end}];
        DPAmp = [TD{row,17:2:end}];
        DPIdx = 1:min(length(DPLat),length(DPAmp));
        
        for k = 1:length(Idx)
            hTmpWaveMarker(k) = line(Lat(Idx(k)),...
                Amp(Idx(k)),...
                'marker','o','color','r','linewidth',1,'linestyle','none','markersize',4,'parent',ha(1));
            hTmpWaveText(k) = text(Lat(Idx(k)),...
                Amp(Idx(k))+0.1,...
                WaveText{k},'HorizontalAlignment','center','VerticalAlignment','bottom',...
                'fontsize',10,'fontweight','bold','parent',ha(1));
        end
        if strcmp(TD{row,4},' B') && ~isempty(BDPLat) && ~isempty(BDPAmp)
            hTmpBDPMarker = line(BDPLat(BDPIdx),...
                BDPAmp(BDPIdx),...
                'marker','x','color','r','linewidth',1,'linestyle','none','markersize',8,'parent',ha(2));
            hTmpBDPText = text(BDPLat(BDPIdx),...
                BDPAmp(BDPIdx)+0.1,...
                'BDP','HorizontalAlignment','center','VerticalAlignment','bottom',...
                'fontsize',10,'fontweight','bold','parent',ha(2));
        end
        if strcmp(TD{row,4},' B') && ~isempty(DPLat) && ~isempty(DPAmp)
            for k = 1:length(DPIdx)
                hTmpDPMarker = line(DPLat(DPIdx(k)),...
                    DPAmp(DPIdx(k)),...
                    'marker','^','color','r','linewidth',1,'linestyle','none','markersize',8,'parent',ha(2));
                hTmpDPText = text(DPLat(DPIdx(k)),...
                    DPAmp(DPIdx(k))+0.1,...
                    sprintf('DP%1.0f',k),'HorizontalAlignment','center','VerticalAlignment','bottom',...
                    'fontsize',10,'fontweight','bold','parent',ha(2));
            end
        end
        
        set(hTmpWaveLine,'XData',time,'YData',data);
        xlabel('latency / ms');
        %     ylabel('ABR amplitude / µV');
        set(ha(1),'ylim',([-1 1]*1.2*ceil((max(abs(data)))/2)*2));
        set(ha,'xlim',([max(min(time),handles.Settings.TimeLimits(1)) min(max(time),handles.Settings.TimeLimits(2))]));
        set(ha,'xtick',(handles.Settings.TimeLimits(1):0.5:handles.Settings.TimeLimits(2)),'ytick',-10:1:10);
        grid on;
        
        % sum of left and right (or other channel if left/right)
        if ~isempty(sumdata)
            set(hTmpSumLine,'XData',time,'YData',sumdata);
        else
            set(hTmpSumLine,'XData',[],'YData',[]);
        end
        
        % bdp
        if ~isempty(bdpdata)
            set(hTmpBDPLine,'XData',time,'YData',bdpdata);
            set(ha(2),'ylim',([-1 1]*1.2*ceil((max(abs(bdpdata)))/2)*2));
        else
            set(hTmpBDPLine,'XData',[],'YData',[]);
            set(ha(2),'ylim',([-1 1]*1.2*ceil((max(abs(data))))/10));
            set(ha(2),'yticklabel',{});
        end
        
        if ypos < MaxAxPerFig
            set(ha,'xticklabel',{});
            xlabel('');
        end
        
        set(ha,'position',[0.05 max(0,(MaxAxPerFig-ypos+0.7)*(1/(MaxAxPerFig+1))) 0.9 0.95/(MaxAxPerFig+1)]);
        set(ha,'box','on','fontsize',10);
        set(ha(1),'xgrid','on','ygrid','on');
        set(ha(2),'xgrid','off','ygrid','off');
        
        if strcmp(TD{row,4},' B')
            text(-2.5,0,sprintf('ITD = %8.1f µs\nILD = %6.1f dB\nLevel = %6.1f dB SPL',TD{row,2},TD{row,3},TD{row,5}),...
                'fontsize',8,'horizontalalignment','left','verticalalignment','middle','linestyle','-',...
                'backgroundcolor','w','margin',5);%,'erasemode','background');
        else
            text(-2.5,0,sprintf('%s\nILD = %6.1f dB\nLevel = %6.1f dB SPL',TD{row,4},TD{row,3},TD{row,5}),...
                'fontsize',8,'horizontalalignment','left','verticalalignment','middle','linestyle','-',...
                'backgroundcolor','w','margin',5);%,'erasemode','background');
        end
        
        ymaxlim = max([ymaxlim;abs(cell2mat(get(ha,'ylim'))).']);
        hAx = [hAx; ha];
        
    end
    
    for k = 1:2
        set(hAx(:,k),'ylim',[-1 1]*ymaxlim(k));
        if ymaxlim(k) > 10
            set(hAx(:,k),'ytick',-100:10:100);
        elseif ymaxlim(k) > 4
            set(hAx(:,k),'ytick',-10:2:10);
        elseif ymaxlim < 2
            set(hAx(:,k),'ytick',-10:0.5:10);
        else
            set(hAx(:,k),'ytick',-10:1:10);
        end
    end
    
    
function mnLatAmp_Callback(hObject, eventdata, handles)
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Settings menu
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function mnSettings_Callback(hObject, eventdata, handles)
    
    
function mnShowMinima_Callback(hObject, eventdata, handles)
    if strcmp(get(hObject, 'Checked'), 'on')
        set(hObject, 'Checked', 'off');
    else
        set(hObject, 'Checked', 'on');
    end
    
    for k = 1:length(handles.min_markers)
        if isvalid(handles.min_markers(k))
            if strcmp(get(hObject, 'Checked'), 'on')
                handles.min_markers(k).enable_feature;
            else
                handles.min_markers(k).disable_feature;
            end
        end
    end
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Options menu
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function mnOptions_Callback(hObject, eventdata, handles)
    
    
function mnAddDPMark_Callback(hObject, eventdata, handles)
    if strcmp(get(hObject, 'Checked'), 'off')
        set(hObject, 'Checked', 'on');
        set(handles.DPMarker, 'visible', 'on');
        set(handles.DPText, 'visible', 'on');
    else
        set(hObject, 'Checked', 'off');
        set(handles.DPMarker, 'visible', 'off');
        set(handles.DPText, 'visible', 'off');
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Visible UI components
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function lbFiles_Callback(hObject, eventdata, handles)
    FileList = cellstr(get(hObject,'String'));
    Index    = get(hObject,'Value');
    if length(Index) > 1
        Index = Index(1);
        set(hObject,'Value',Index);
    end
    if ~strcmp(FileList{Index},'...')
        if handles.LastIndex > 0
            handles.Table{handles.LastIndex,1} = get(handles.tblData,'Data');
            [handles.Table{handles.LastIndex,1}{:,1}] = deal([]);
        end
        handles.LastIndex = Index;
        
        set([handles.tblData handles.lbFiles],'enable','off');
        
        %% simple, no merging
        % handles.Data = load(fullfile(handles.CurrentPath,FileList{Index}));
        
        %% merge data files
        Avg  = 0;
        AvgC = 0;
        Mic  = 0;
        MicC = 0;
        ITD = [];
        ILD = [];
        Fs = [];
        Level = [];
        
        for fx = 1:length(handles.FileNames{Index})
            
            tmp = load(fullfile(handles.CurrentPath,handles.FileNames{Index}{fx}));
            
            [tmp.St.ITD,ITDorder] = sort(tmp.St.ITD);
            [tmp.St.ILD,ILDorder] = sort(tmp.St.ILD);
            
            tmp.Avg  = tmp.Avg(:,[1 2 ITDorder+2],:);
            tmp.Avg  = tmp.Avg(:,:,ILDorder);
            
            tmp.AvgC = tmp.AvgC([1 2 ITDorder+2],:);
            tmp.AvgC = tmp.AvgC(:,ILDorder);
            
            tmp.Mic  = tmp.Mic(:,:,[1 2 ITDorder+2],:);
            tmp.Mic  = tmp.Mic(:,:,:,ILDorder);
            
            tmp.MicC = tmp.MicC([1 2 ITDorder+2],:);
            tmp.MicC = tmp.MicC(:,ILDorder);
            
            if tmp.St.Fs == 96000
                answer = questdlg('The sampling rate saved in the file is 96000 Hz. This might be wrong. Do you want to correct it to 48000 Hz?', ...
                    'Sampling rate wrong', ...
                    'Yes (48000 Hz)', 'No (96000 Hz)', 'Yes (48000 Hz)');
                if strcmp(answer, 'Yes (48000 Hz)')
                    tmp.St.Fs = 48000;
                end
            end
            
            if ~isempty(Fs) && ~all(Fs == tmp.St.Fs)
                set([handles.tblData handles.lbFiles],'enable','on');
                error('experiments do not match: sampling rate');
            else
                Fs = tmp.St.Fs;
            end
            
            if ~isempty(Level) && ~all(Level == tmp.St.Level)
                set([handles.tblData handles.lbFiles],'enable','on');
                error('experiments do not match: level');
            else
                Level = tmp.St.Level;
            end
            
            if strcmp(tmp.St.Type, 'tone')
                if ~isempty(handles.StimType) && ~strcmp(handles.StimType, tmp.St.Type) && handles.StimFreq ~= tmp.St.Frequency
                    set([handles.tblData handles.lbFiles],'enable','on');
                    error('experiments do not match: stimulus type and/or frequency');
                end
                handles.StimType = 'tone';
                handles.StimFreq = tmp.St.Frequency;
            end
            
            
            %         if ~isempty(ITD) && ~all(ITD == tmp.St.ITD)
            %             set([handles.tblData handles.lbFiles],'enable','on');
            %             error('experiments do not match: ITD list');
            %         else
            %             ITD = tmp.St.ITD;
            %         end
            %
            %         if ~isempty(ILD) && ~all(ILD == tmp.St.ILD)
            %             set([handles.tblData handles.lbFiles],'enable','on');
            %             error('experiments do not match: ILD list');
            %         else
            %             ILD = tmp.St.ILD;
            %         end
            
            if isempty(ITD) && isempty(ILD)
                ITD = tmp.St.ITD(:);
                ILD = tmp.St.ILD(:);
                Avg  = zeros(size(tmp.Avg,1),2+length(ITD),length(ILD));
                AvgC = zeros(2+length(ITD),length(ILD));
                Mic  = zeros(size(tmp.Mic,1),size(tmp.Mic,2),2+length(ITD),length(ILD));
                MicC = zeros(2+length(ITD),length(ILD));
            end
            
            
            for lx = 1:length(tmp.St.ILD)
                % insert new ILD column if necessary
                if ~ismember(tmp.St.ILD(lx),ILD)
                    pos = find(ILD<tmp.St.ILD(lx),1,'last');
                    if isempty(pos)
                        pos = 0;
                    end
                    ILD  = cat(1,ILD(1:pos),tmp.St.ILD(lx),ILD(pos+1:end));
                    Avg  = cat(3, Avg(:  ,:,1:pos),zeros(size(Avg,1),            size(Avg ,2),1), Avg(:  ,:,pos+1:end));
                    AvgC = cat(2,AvgC(    :,1:pos),zeros(                        size(AvgC,1),1),AvgC(    :,pos+1:end));
                    Mic  = cat(4, Mic(:,:,:,1:pos),zeros(size(Mic,1),size(Mic,2),size(Mic ,3),1), Mic(:,:,:,pos+1:end));
                    MicC = cat(2,MicC(    :,1:pos),zeros(                        size(MicC,1),1),MicC(    :,pos+1:end));
                end
                
                % left channel (index 1)
                tpos = 1;                         t_tpos = 1;
                lpos = find(ILD==tmp.St.ILD(lx)); t_lpos = find(tmp.St.ILD==tmp.St.ILD(lx));
                Avg(:,  tpos,lpos) =  Avg(:,  tpos,lpos) + tmp.AvgC(t_tpos,t_lpos) .* tmp.Avg(:,  t_tpos,t_lpos);
                AvgC(    tpos,lpos) = AvgC(    tpos,lpos) + tmp.AvgC(t_tpos,t_lpos);
                Mic(:,:,tpos,lpos) =  Mic(:,:,tpos,lpos) + tmp.MicC(t_tpos,t_lpos) .* tmp.Mic(:,:,t_tpos,t_lpos);
                MicC(    tpos,lpos) = MicC(    tpos,lpos) + tmp.MicC(t_tpos,t_lpos);
                
                % right channel (index 2)
                tpos = 2;                         t_tpos = 2;
                lpos = find(ILD==tmp.St.ILD(lx)); t_lpos = find(tmp.St.ILD==tmp.St.ILD(lx));
                Avg(:,  tpos,lpos) =  Avg(:,  tpos,lpos) + tmp.AvgC(t_tpos,t_lpos) .* tmp.Avg(:,  t_tpos,t_lpos);
                AvgC(    tpos,lpos) = AvgC(    tpos,lpos) + tmp.AvgC(t_tpos,t_lpos);
                Mic(:,:,tpos,lpos) =  Mic(:,:,tpos,lpos) + tmp.MicC(t_tpos,t_lpos) .* tmp.Mic(:,:,t_tpos,t_lpos);
                MicC(    tpos,lpos) = MicC(    tpos,lpos) + tmp.MicC(t_tpos,t_lpos);
                
                for tx = 1:length(tmp.St.ITD)
                    
                    % insert new ITD column if necessary
                    if ~ismember(tmp.St.ITD(tx),ITD)
                        pos = find(ITD<tmp.St.ITD(tx),1,'last');
                        if isempty(pos)
                            pos = 0;
                        end
                        ITD  = cat(1,ITD(1:pos),tmp.St.ITD(tx),ITD(pos+1:end));
                        pos = pos + 2;
                        Avg  = cat(2, Avg(:  ,1:pos,:),zeros(size(Avg,1),            1,size(Avg ,3)), Avg(:  ,pos+1:end,:));
                        AvgC = cat(1,AvgC(    1:pos,:),zeros(                        1,size(AvgC,2)),AvgC(    pos+1:end,:));
                        Mic  = cat(3, Mic(:,:,1:pos,:),zeros(size(Mic,1),size(Mic,2),1,size(Mic ,4)), Mic(:,:,pos+1:end,:));
                        MicC = cat(1,MicC(    1:pos,:),zeros(                        1,size(MicC,2)),MicC(    pos+1:end,:));
                    end
                    
                    % position in summary Avg etc     % position in tmp.Avg etc.
                    tpos = 2+find(ITD==tmp.St.ITD(tx)); t_tpos = 2+find(tmp.St.ITD==tmp.St.ITD(tx));
                    lpos =   find(ILD==tmp.St.ILD(lx)); t_lpos =   find(tmp.St.ILD==tmp.St.ILD(lx));
                    Avg(:,  tpos,lpos) =  Avg(:,  tpos,lpos) + bsxfun(@times,permute(tmp.AvgC(t_tpos,t_lpos),[3 1 2]),tmp.Avg(:,  t_tpos,t_lpos));
                    AvgC(    tpos,lpos) = AvgC(    tpos,lpos) + tmp.AvgC(t_tpos,t_lpos);
                    Mic(:,:,tpos,lpos) =  Mic(:,:,tpos,lpos) + bsxfun(@times,permute(tmp.MicC(t_tpos,t_lpos),[3 4 1 2]),tmp.Mic(:,:,t_tpos,t_lpos));
                    MicC(    tpos,lpos) = MicC(    tpos,lpos) + tmp.MicC(t_tpos,t_lpos);
                    
                end
            end
            
            %         Avg  =  Avg + bsxfun(@times,permute(tmp.AvgC,[3 1 2]),tmp.Avg);
            %         AvgC = AvgC + tmp.AvgC;
            %         Mic  =  Mic + bsxfun(@times,permute(tmp.MicC,[3 4 1 2]),tmp.Mic);
            %         MicC = MicC + tmp.MicC;
            
        end
        
        Avg = bsxfun(@rdivide,Avg,permute(AvgC,[3 1 2]));
        Mic = bsxfun(@rdivide,Mic,permute(MicC,[3 4 1 2]));
        
        % copy merged data back to handles structure
        handles.Data = struct('Avg',Avg,'Mic',Mic,'Fs',Fs,'ITD',ITD,'ILD',ILD,'Level',Level);
        %%
        
        set([handles.tblData handles.lbFiles],'enable','on');
        D = handles.Data;
        
        if Index <= size(handles.Table,1) && ~isempty(handles.Table{Index,1})
            set(handles.tblData,'Data',handles.Table{Index,1});
        else
            TD = cell(1,15);
            set(handles.tblData,'Data',TD);
            r = 0; Side = {' L',' R',' S',' B'};
            for tx = 1:length(D.ITD)
                for lx = 1:length(D.ILD)
                    if D.ITD(tx) == 0 || length(D.ITD) == 1
                        for sx = 1:length(Side)
                            r = r + 1;
                            TD{r,1} = [];
                            if strcmp(Side{sx}, ' B')
                                TD{r,2} = D.ITD(tx)/1e-6;
                            else
                                TD{r,2} = 0;
                            end
                            TD{r,3} = D.ILD(lx);
                            TD{r,4} = Side{sx};
                            TD{r,5} = handles.Data.Level;
                        end
                    else
                        sx = strcmp(Side,' B');
                        r = r + 1;
                        TD{r,1} = [];
                        TD{r,2} = D.ITD(tx)/1e-6;
                        TD{r,3} = D.ILD(lx);
                        TD{r,4} = Side{sx};
                        TD{r,5} = handles.Data.Level;
                    end
                end
            end
            [~,sortidx] = sortrows([abs([TD{:,2}].') abs([TD{:,3}].')]);
            set(handles.tblData,'Data',TD(sortidx,:));
        end
        
        handles.CurrentRow = [];
        guidata(hObject,handles);
        set(handles.axWaveForm,'Xlim',[0 1e-10],'ylim',[0 1e-10]);
        tblData_CellSelectionCallback(handles.tblData, [], handles);
    else
        mnOpen_Callback(handles.mnOpen, eventdata, handles);
    end
    
    
    
function tblData_CellSelectionCallback(hObject, eventdata, handles)
    if (isfield(eventdata,'Indices') || isa(eventdata, 'matlab.ui.eventdata.CellSelectionChangeData') ...
            && ~isempty(eventdata.Indices)) && size(eventdata.Indices,1) > 0
        if ~isempty(handles.CurrentRow) && handles.CurrentRow > 0
            handles.Table{handles.LastIndex,2}{handles.CurrentRow,1} = handles.Candidates.Lat;
            handles.Table{handles.LastIndex,2}{handles.CurrentRow,2} = handles.Candidates.Amp;
            handles.Table{handles.LastIndex,2}{handles.CurrentRow,3} = handles.Candidates.Idx;
            handles.Table{handles.LastIndex,2}{handles.CurrentRow,4} = handles.Candidates.BDPLat;
            handles.Table{handles.LastIndex,2}{handles.CurrentRow,5} = handles.Candidates.BDPAmp;
            handles.Table{handles.LastIndex,2}{handles.CurrentRow,6} = handles.Candidates.BDPIdx;
            handles.Table{handles.LastIndex,2}{handles.CurrentRow,7} = handles.Candidates.DPLat;
            handles.Table{handles.LastIndex,2}{handles.CurrentRow,8} = handles.Candidates.DPAmp;
            handles.Table{handles.LastIndex,2}{handles.CurrentRow,9} = handles.Candidates.DPIdx;
        end
        
        handles.CurrentRow = eventdata.Indices(end,1);
        
        % table marker
        TD = get(handles.tblData,'Data');
        [TD{:,1}] = deal([]);
        TD{handles.CurrentRow,1} = 'X';

        set(handles.tblData,'Data',TD);
        restoreView(handles);
        
        
        % waveform
        [data,time,bdpdata,sumdata] = PrepareData(handles,handles.CurrentRow);
        set(handles.WaveLine,'XData',time,'YData',data);
        xlabel('latency / ms');
        ylabel('ABR amplitude / µV');
        ylim([-1 1]*1.2*max(abs(data)));
        xlim([max(min(time),handles.Settings.TimeLimits(1)) min(max(time),handles.Settings.TimeLimits(2))]);
        set(gca,'xtick',(handles.Settings.TimeLimits(1):0.5:handles.Settings.TimeLimits(2)),'ytick',-10:1:10);
        grid on;
        
        if ~isempty(handles.min_markers)
            delete(handles.min_markers(isvalid(handles.min_markers)));
        end
        handles.min_markers(1) = feature_marker(time, data, handles.axWaveForm, 'min', 'v');
        handles.min_markers(2) = feature_marker(time, data, handles.axWaveForm, 'min', 'v');
        
        
        % sum of left and right (or other channel if left/right)
        if ~isempty(sumdata)
            set(handles.SumLine,'XData',time,'YData',sumdata);
            handles.min_markers(3) = feature_marker(time, sumdata, handles.axWaveForm, 'min', 'v');
            handles.min_markers(4) = feature_marker(time, sumdata, handles.axWaveForm, 'min', 'v');
        else
            set(handles.SumLine,'XData',[],'YData',[]);
        end
        for k = 1:length(handles.min_markers)
            if isvalid(handles.min_markers(k))
                if strcmp(get(handles.mnShowMinima, 'Checked'), 'off')
                    handles.min_markers(k).disable_feature;
                else
                    handles.min_markers(k).enable_feature;
                end
            end
        end
        
        
        % bdp
        if ~isempty(bdpdata)
            set(handles.BDPLine,'XData',time,'YData',bdpdata);
        else
            set(handles.BDPLine,'XData',[],'YData',[]);
        end
        
        % maxima
        if     handles.LastIndex  <= size(handles.Table,1) ...
                && ~isempty(handles.Table{handles.LastIndex,2}) ...
                && handles.CurrentRow <= size(handles.Table{handles.LastIndex,2},1) ...
                && ~isempty(handles.Table{handles.LastIndex,2}{handles.CurrentRow,1})
            handles.Candidates.Lat = handles.Table{handles.LastIndex,2}{handles.CurrentRow,1};
            handles.Candidates.Amp = handles.Table{handles.LastIndex,2}{handles.CurrentRow,2};
            handles.Candidates.Idx = handles.Table{handles.LastIndex,2}{handles.CurrentRow,3};
            handles.Candidates.BDPLat = handles.Table{handles.LastIndex,2}{handles.CurrentRow,4};
            handles.Candidates.BDPAmp = handles.Table{handles.LastIndex,2}{handles.CurrentRow,5};
            handles.Candidates.BDPIdx = handles.Table{handles.LastIndex,2}{handles.CurrentRow,6};
            if size(handles.Table{handles.LastIndex,2},2) >= 9
                handles.Candidates.DPLat = handles.Table{handles.LastIndex,2}{handles.CurrentRow,7};
                handles.Candidates.DPAmp = handles.Table{handles.LastIndex,2}{handles.CurrentRow,8};
                handles.Candidates.DPIdx = handles.Table{handles.LastIndex,2}{handles.CurrentRow,9};
            else
                handles.Candidates.DPLat = [];
                handles.Candidates.DPAmp = [];
                handles.Candidates.DPIdx = [];
                [~,~,~,~,~,~,dplat,dpamp,dpstartidx] = GetMaxima(time,data,bdpdata);
                handles.Candidates.DPLat = dplat;
                handles.Candidates.DPAmp = dpamp;
                if isempty(dpstartidx)
                    handles.Candidates.DPIdx = 1:handles.NumDPs;
                else
                    tmpidx = find(handles.Candidates.DPLat<handles.Candidates.BDPLat(handles.Candidates.BDPIdx), 1, 'last');
                    if ~isempty(tmpidx)
                        handles.Candidates.DPIdx(1) = tmpidx;
                    else
                        handles.Candidates.DPIdx(1) = 1;
                    end
                    tmpidx = find(handles.Candidates.DPLat>handles.Candidates.BDPLat(handles.Candidates.BDPIdx), 1, 'first');
                    if ~isempty(tmpidx)
                        handles.Candidates.DPIdx(2) = tmpidx;
                    else
                        handles.Candidates.DPIdx(2) = length(handles.Candidates.DPLat);
                    end
                    handles.Candidates.DPIdx(end+(1:handles.NumDPs-2)) = handles.Candidates.DPIdx(2)+(1:handles.NumDPs-2);
                end
            end
        else
            [lat,amp,startidx,bdplat,bdpamp,bdpstartidx,dplat,dpamp,dpstartidx] = GetMaxima(time,data,bdpdata);
            handles.Candidates.Lat = lat;
            handles.Candidates.Amp = amp;
            if isempty(startidx)
                handles.Candidates.Idx = 1:handles.NumWaves;
            else
                startidx = min(startidx, length(handles.Candidates.Lat) - handles.NumWaves + 1);
                handles.Candidates.Idx = startidx+(0:handles.NumWaves-1);
            end
            handles.Candidates.BDPLat = bdplat;
            handles.Candidates.BDPAmp = bdpamp;
            [~,handles.Candidates.BDPIdx] = min(abs(bdplat-lat(min(4,end))));
            handles.Candidates.DPLat = dplat;
            handles.Candidates.DPAmp = dpamp;
            if isempty(dpstartidx)
                handles.Candidates.DPIdx = 1:handles.NumDPs;
            else
                tmpidx = find(handles.Candidates.DPLat<handles.Candidates.BDPLat(handles.Candidates.BDPIdx), 1, 'last');
                if ~isempty(tmpidx)
                    handles.Candidates.DPIdx(1) = tmpidx;
                else
                    handles.Candidates.DPIdx(1) = 1;
                end
                tmpidx = find(handles.Candidates.DPLat>handles.Candidates.BDPLat(handles.Candidates.BDPIdx), 1, 'first');
                if ~isempty(tmpidx)
                    handles.Candidates.DPIdx(2) = tmpidx;
                else
                    handles.Candidates.DPIdx(2) = length(handles.Candidates.DPLat);
                end
                handles.Candidates.DPIdx(end+(1:handles.NumDPs-2)) = handles.Candidates.DPIdx(2)+(1:handles.NumDPs-2);
            end
        end
        
        guidata(hObject,handles);
        PlotWaves(handles);
        FillTable(handles);
    end
    
    
    
function [data,time,bdpdata,sumdata] = PrepareData(handles,tablerow)
    TD = get(handles.tblData,'Data');
    ITD = TD{tablerow,2};
    nZeros = ceil(max(abs([TD{:,2}]*1e-6))*handles.Data.Fs);
    ILD = TD{tablerow,3};
    Side = TD{tablerow,4};
    if strcmpi(Side,' L')
        ITDidx = 1;
    elseif strcmpi(Side,' R')
        ITDidx = 2;
    else
        [~,ITDidx] = min(abs(ITD*1e-6-handles.Data.ITD));
        ITDidx = ITDidx + 2;
    end
    [~,ILDidx] = min(abs(ILD-handles.Data.ILD));

    if strcmpi(Side,' S')
        data = handles.Data.Avg(:,1,ILDidx)+handles.Data.Avg(:,2,ILDidx);
    else
        data = handles.Data.Avg(:,ITDidx,ILDidx);
    end
   
    time = (0:size(data,1)-1).'/handles.Data.Fs/1e-3;
    freq = (0:size(data,1)+nZeros-1).'/(size(data,1)+nZeros)*handles.Data.Fs;
    freq(freq>=handles.Data.Fs/2) = freq(freq>=handles.Data.Fs/2) - handles.Data.Fs;
    
    tmp   = real(ifft(fft([handles.Data.Avg(:,1,ILDidx);zeros(nZeros,1)]).*exp(-1i*2*pi*(+ITD*1e-6)/2*freq)));
    left  = tmp(1:size(data,1));
    tmp   = real(ifft(fft([handles.Data.Avg(:,2,ILDidx);zeros(nZeros,1)]).*exp(-1i*2*pi*(-ITD*1e-6)/2*freq)));
    right = tmp(1:size(data,1));
    
    if strcmpi(Side,' L')
        bdpdata = [];
        sumdata = right;
    elseif strcmpi(Side,' R')
        bdpdata = [];
        sumdata = left;
    elseif strcmpi(Side,' S')
        bdpdata = [];
        sumdata = [];
    elseif strcmpi(Side,'BS')
        sumdata = data;
        data = left+right;
        bdpdata = sumdata-data;
    else
        sumdata = left+right;
        bdpdata = data-sumdata;
    end
    
    % filtering
    if strcmpi(handles.Settings.FilterType,'FIR')
        b = fir1(handles.Settings.FIRFilterOrder,handles.Settings.FilterCutOff/handles.Data.Fs*2);
        a = 1;
    elseif strcmpi(handles.Settings.FilterType,'IIR')
        [b,a] = butter(handles.Settings.IIRFilterOrder,handles.Settings.FilterCutOff/handles.Data.Fs*2);
    elseif strcmpi(handles.Settings.FilterType,'none')
        b = 1;
        a = 1;
    else
        fprintf('filter type not implemented\n');
        b = 1;
        a = 1;
    end
    data     = filter(b,a,data);
    sumdata  = filter(b,a,sumdata);
    bdpdata  = filter(b,a,bdpdata);
    
    gd = mean(grpdelay(b,a,linspace(min(handles.Settings.FilterCutOff),max(handles.Settings.FilterCutOff),101),handles.Data.Fs));
    
    % zero
    if isempty(handles.StimType)
        [mx,zidx] = max(abs(handles.Data.Mic(:,1:2,ITDidx,ILDidx)));
    else
        % empirically determined
        filter_order = max(64,ceil(4*handles.Data.Fs/handles.StimFreq));
        bf = fir1(filter_order,handles.StimFreq*2.^([-1 1]/4)/handles.Data.Fs*2);
        w = hann(round(max(0.001,4/handles.StimFreq)*handles.Data.Fs)*2);
        w = [w(1:end/2);ones(size(handles.Data.Mic,1)-length(w),1);w(end/2+1:end)];
        tmpsig = fftfilt(bf,bsxfun(@times, w, handles.Data.Mic(:,1:2,ITDidx,ILDidx)));
        mx = std(tmpsig);
        zidx = ones(1,2);
        for ch = 1:2
            zidx(ch) = find(tmpsig(:,ch) > mx(ch), 1, 'first') - filter_order/2;
        end
    end
    if ITDidx == 1
        zidx = zidx(1);
    elseif ITDidx == 2
        zidx = zidx(2);
    elseif ILDidx == 1
        zidx = min(zidx);
    else
        [~,mxidx] = max(mx);
        zidx = zidx(mxidx);
    end
    
    time = time - zidx/handles.Data.Fs/1e-3 - gd/handles.Data.Fs/1e-3;
    
    
function [lat,amp,idx,bdplat,bdpamp,bdpidx,dplat,dpamp,dpidx] = GetMaxima(time,data,bdpdata)
    % running average DC removal window
    M = round(0.006/(time(2)-time(1))*1e3);
    % wave maxima
    if  M < size(data,1)/3
        D   = diff(sign(diff(data-filtfilt(rectwin(M)/M,1,data))));
    else
        D   = diff(sign(diff(data-mean(data))));
    end
    pos = find(D<0)+1;
    s   = std(data(time<0));
    
    pos(time(pos)<0 | time(pos)>max(time)) = [];
    idx = find(data(pos)>3*s,1,'first');
    
    lat = time(pos);
    amp = data(pos);
    
    
    % bdp maxima
    if ~isempty(bdpdata)
        if  M < size(bdpdata,1)/3
            D   = diff(sign(diff(bdpdata-filtfilt(rectwin(M)/M,1,bdpdata))));
        else
            D   = diff(sign(diff(bdpdata-mean(bdpdata))));
        end
        % BDP
        pos = find(D>0)+1;
        s   = std(bdpdata(time<0));
        
        pos(time(pos)<0 | time(pos)>max(time)) = [];
        bdpidx = find(data(pos)>3*s,1,'first');
        
        bdplat = time(pos);
        bdpamp = bdpdata(pos);
        % DP1..n
        pos = find(D<0)+1;
        s   = std(bdpdata(time<0));
        
        pos(time(pos)<0 | time(pos)>max(time)) = [];
        if isempty(bdpidx)
            bdpidx = 1;
        end
        dpidx = find(time(pos)<bdplat(bdpidx),1,'last');
        if isempty(dpidx)
            dpidx = 1;
        end
        
        dplat = time(pos);
        dpamp = bdpdata(pos);
    else
        bdpidx = [];
        bdplat = [];
        bdpamp = [];
        dpidx = [];
        dplat = [];
        dpamp = [];
    end
    
    
function figABRAnalyser_WindowButtonMotionFcn(hObject, eventdata, handles)
    wcoord = get(hObject,'CurrentPoint');
    wpos = get(hObject,'Position');
    apos = get(handles.axWaveForm,'Position');
    alim = [get(handles.axWaveForm,'Xlim'); get(handles.axWaveForm,'Ylim')];
    x = alim(1,1)+(alim(1,2)-alim(1,1))*(wcoord(1)/wpos(3)-apos(1))/apos(3);
    y = alim(2,1)+(alim(2,2)-alim(2,1))*(wcoord(2)/wpos(4)-apos(2))/apos(4);
    if handles.CursorMode
        hmark = findall([handles.WaveMarker handles.BDPMarker handles.DPMarker],'linewidth',3);
        if isempty(hmark) || hmark ~= handles.BDPMarker && ~ismember(hmark, handles.DPMarker)
            value = interp1(get(handles.WaveLine,'XData'),get(handles.WaveLine,'YData'),x);
        elseif hmark == handles.BDPMarker || ismember(hmark, handles.DPMarker)
            value = interp1(get(handles.BDPLine,'XData'),get(handles.BDPLine,'YData'),x);
        end
        set(handles.CursorLines(1),'XData',[alim(1,1);alim(1,2)],'YData',[value;value]);
        set(handles.CursorLines(2),'XData',[x;x],'YData',[alim(2,1);alim(2,2)]);
        set(handles.CursorText,'position',[x+0.3 value-0.2 0],'String',sprintf('%9.6f ms\n%9.6f µV',x,value));
        if ~isempty(hmark)
            set(hmark,'XData',x,'YData',value);
            if hmark == handles.BDPMarker
                set(handles.BDPText,'Position',[x value+0.1 0]);
            elseif ismember(hmark, handles.DPMarker)
                set(handles.DPText(handles.DPMarker == hmark),...
                    'Position',[x value+0.1 0]);
            else
                set(handles.WaveText(handles.WaveMarker == hmark),...
                    'Position',[x value+0.1 0]);
            end
        end
    else
        xdata = get(handles.WaveMarker,'xdata');
        ydata = get(handles.WaveMarker,'ydata');
        [mn,ix] = min(([xdata{:}]-x).^2+([ydata{:}]-y).^2);
        xdata = get(handles.BDPMarker,'xdata');
        ydata = get(handles.BDPMarker,'ydata');
        mnb = min((xdata-x).^2+(ydata-y).^2);
        xdata = get(handles.DPMarker,'xdata');
        ydata = get(handles.DPMarker,'ydata');
        [mnd,ixd] = min(([xdata{:}]-x).^2+([ydata{:}]-y).^2);
        
        set(handles.WaveMarker,'linewidth',0.5);
        set(handles.BDPMarker,'linewidth',0.5);
        set(handles.DPMarker,'linewidth',0.5);
        
        dist = Inf;
        for m = 1:length(handles.min_markers)
            if isvalid(handles.min_markers(m))
                handles.min_markers(m).unselect_feature;
                dist(m) = handles.min_markers(m).get_distance(x,y);
            else
                dist(m) = Inf;
            end
        end
        
        if mn < mnb && mn < mnd && all(all(bsxfun(@lt,mn,dist.'))) && mn < 2
            set(handles.WaveMarker(ix),'linewidth',3);
        elseif all(mnb < dist) && mnb < mnd && mnb < 2
            set(handles.BDPMarker,'linewidth',3);
        elseif mnd < 2
            set(handles.DPMarker(ixd),'linewidth',3);
        else
            [~,ix] = min(dist);
            if length(handles.min_markers) >= ix && isvalid(handles.min_markers(ix))
                handles.min_markers(ix).select_feature;
            end
        end
    end
    
    
    
    
function figABRAnalyser_WindowKeyPressFcn(hObject, eventdata, handles)
    switch eventdata.Key
        case 'f'
            if ~handles.CursorMode
                handles.CursorMode = true;
                set(handles.CursorLines,'Visible','on');
                set(handles.CursorText,'Visible','on');
            end
            guidata(hObject,handles);
        case 'e'
            if handles.CursorMode
                handles.CursorMode = false;
                set(handles.CursorLines,'Visible','off');
                set(handles.CursorText,'Visible','off');
                hmark = findall([handles.WaveMarker handles.BDPMarker handles.DPMarker],'linewidth',3);
                if ~isempty(hmark)
                    if hmark == handles.BDPMarker
                        handles.Candidates.BDPLat(end+1) = get(hmark,'XData');
                        [handles.Candidates.BDPLat,sortorder] = sort(handles.Candidates.BDPLat);
                        handles.Candidates.BDPIdx = find(sortorder==length(sortorder));
                        handles.Candidates.BDPAmp(end+1) = get(hmark,'YData');
                        handles.Candidates.BDPAmp = handles.Candidates.BDPAmp(sortorder);
                    elseif ismember(hmark, handles.DPMarker)
                        k = find(handles.DPMarker == hmark);
                        handles.Candidates.DPLat(end+1) = get(hmark,'XData');
                        [handles.Candidates.DPLat,sortorder] = sort(handles.Candidates.DPLat);
                        handles.Candidates.DPIdx(k) = find(sortorder==length(sortorder));
                        others = setdiff(1:length(handles.Candidates.DPIdx),k);
                        for m = 1:length(others)
                            handles.Candidates.DPIdx(others(m)) = find(sortorder==handles.Candidates.DPIdx(others(m)));
                        end
                        handles.Candidates.DPAmp(end+1) = get(hmark,'YData');
                        handles.Candidates.DPAmp = handles.Candidates.DPAmp(sortorder);
                    else
                        k = find(handles.WaveMarker == hmark);
                        handles.Candidates.Lat(end+1) = get(hmark,'XData');
                        [handles.Candidates.Lat,sortorder] = sort(handles.Candidates.Lat);
                        handles.Candidates.Idx(k) = find(sortorder==length(sortorder));
                        others = setdiff(1:length(handles.Candidates.Idx),k);
                        for m = 1:length(others)
                            handles.Candidates.Idx(others(m)) = find(sortorder==handles.Candidates.Idx(others(m)));
                        end
                        handles.Candidates.Amp(end+1) = get(hmark,'YData');
                        handles.Candidates.Amp = handles.Candidates.Amp(sortorder);
                    end
                end
                guidata(hObject,handles);
                PlotWaves(handles);
                FillTable(handles);
            end
        case 'escape'
            handles.CursorMode = false;
            guidata(hObject,handles);
            set(handles.CursorLines,'Visible','off');
            set(handles.CursorText,'Visible','off');
            PlotWaves(handles);
            FillTable(handles);
        case 'leftarrow'
            ShiftWave(handles,-1);
        case 'rightarrow'
            ShiftWave(handles,+1);
        case 'uparrow'
            if isempty(handles.CurrentRow) || handles.CurrentRow == 0
                tblData_CellSelectionCallback(handles.tblData, struct('Indices',1), handles);
            elseif handles.CurrentRow > 1
                tblData_CellSelectionCallback(handles.tblData, struct('Indices',handles.CurrentRow-1), handles);
            end
        case 'downarrow'
            if isempty(handles.CurrentRow) || handles.CurrentRow == 0
                tblData_CellSelectionCallback(handles.tblData, struct('Indices',1), handles);
            elseif handles.CurrentRow < size(get(handles.tblData,'Data'),1)
                tblData_CellSelectionCallback(handles.tblData, struct('Indices',handles.CurrentRow+1), handles);
            end
        case 'pageup'
            if isempty(handles.LastIndex) || handles.LastIndex == 0
                set(handles.lbFiles,'Value',1);
                lbFiles_Callback(handles.lbFiles, [], handles);
            elseif handles.LastIndex > 1
                set(handles.lbFiles,'Value',handles.LastIndex-1);
                lbFiles_Callback(handles.lbFiles, [], handles);
            end
        case 'pagedown'
            if isempty(handles.LastIndex) || handles.LastIndex == 0
                set(handles.lbFiles,'Value',1);
                lbFiles_Callback(handles.lbFiles, [], handles);
            elseif handles.LastIndex < length(get(handles.lbFiles,'String'))
                set(handles.lbFiles,'Value',handles.LastIndex+1);
                lbFiles_Callback(handles.lbFiles, [], handles);
            end
        otherwise
            %         disp(eventdata.Key);
    end
    
function lbFiles_KeyPressFcn(hObject, eventdata, handles)
    figABRAnalyser_WindowKeyPressFcn(handles.figABRAnalyser, eventdata, handles)
    
function tblData_KeyPressFcn(hObject, eventdata, handles)
    figABRAnalyser_WindowKeyPressFcn(handles.figABRAnalyser, eventdata, handles)
    
function ShiftWave(handles,direction)
    if strcmp(get(handles.BDPMarker,'visible'),'on') && get(handles.BDPMarker,'linewidth') > 0.5
        tmpidx = handles.Candidates.BDPIdx;
        tmpidx = tmpidx + direction;
        if tmpidx>0 && tmpidx<=length(handles.Candidates.BDPLat)
            handles.Candidates.BDPIdx = tmpidx;
            if ~isempty(handles.Candidates.DPLat) && ~isempty(handles.Candidates.DPIdx)
                if handles.Candidates.DPIdx(1) > 1 ...
                        && handles.Candidates.DPLat(handles.Candidates.DPIdx(1)) >= handles.Candidates.BDPLat(handles.Candidates.BDPIdx)
                    tmpidx = find(handles.Candidates.DPLat < handles.Candidates.BDPLat(handles.Candidates.BDPIdx), 1, 'last');
                    if ~isempty(tmpidx)
                        handles.Candidates.DPIdx(1) = tmpidx;
                    end
                end
                if handles.Candidates.DPIdx(2) < length(handles.Candidates.DPLat) ...
                        && handles.Candidates.DPLat(handles.Candidates.DPIdx(2)) <= handles.Candidates.BDPLat(handles.Candidates.BDPIdx)
                    tmpidx = find(handles.Candidates.DPLat > handles.Candidates.BDPLat(handles.Candidates.BDPIdx), 1, 'first');
                    if ~isempty(tmpidx)
                        handles.Candidates.DPIdx(2) = tmpidx;
                    end
                end
            end
        end
        guidata(handles.figABRAnalyser,handles);
        PlotWaves(handles);
        FillTable(handles);
    elseif any(strcmp(get(handles.DPMarker,'visible'),'on')) && any(cellfun(@(x)x>0.5,get(handles.DPMarker,'linewidth')))
        lw = get(handles.DPMarker,'linewidth');
        ix = find([lw{:}]>0.5,1,'first');
        if ~isempty(ix)
            bLastMoved = false;
            tmpidx = handles.Candidates.DPIdx;
            tmpidx(ix) = tmpidx(ix) + direction;
            if ix == handles.NumDPs
                bLastMoved = true;
            end
            if direction == +1
                for k = ix+1:length(tmpidx)
                    if tmpidx(k)<=tmpidx(k-1)
                        tmpidx(k) = tmpidx(k)+1;
                        if k == handles.NumDPs
                            bLastMoved = true;
                        end
                    end
                end
            else
                for k = ix-1:-1:1
                    if tmpidx(k)>=tmpidx(k+1)
                        tmpidx(k) = tmpidx(k)-1;
                        if k == handles.NumDPs
                            bLastMoved = true;
                        end
                    end
                end
            end
            if all(tmpidx>0) && all(tmpidx<=length(handles.Candidates.DPLat))
                handles.Candidates.DPIdx = tmpidx;
            end
            guidata(handles.figABRAnalyser,handles);
            PlotWaves(handles);
            FillTable(handles);
        end
    elseif any([handles.min_markers(isvalid(handles.min_markers)).is_selected])
        numbers = find(isvalid(handles.min_markers));
        selected = numbers([handles.min_markers(numbers).is_selected]);
        if direction == +1
            handles.min_markers(selected).next_feature;
        else
            handles.min_markers(selected).previous_feature;
        end
    else
        lw = get(handles.WaveMarker,'linewidth');
        ix = find([lw{:}]>0.5,1,'first');
        if ~isempty(ix)
            bWaveIVmoved = false;
            tmpidx = handles.Candidates.Idx;
            tmpidx(ix) = tmpidx(ix) + direction;
            if ix == 4
                bWaveIVmoved = true;
            end
            if direction == +1
                for k = ix+1:length(tmpidx)
                    if tmpidx(k)<=tmpidx(k-1)
                        tmpidx(k) = tmpidx(k)+1;
                        if k == 4
                            bWaveIVmoved = true;
                        end
                    end
                end
            else
                for k = ix-1:-1:1
                    if tmpidx(k)>=tmpidx(k+1)
                        tmpidx(k) = tmpidx(k)-1;
                        if k == 4
                            bWaveIVmoved = true;
                        end
                    end
                end
            end
            if all(tmpidx>0) && all(tmpidx<=length(handles.Candidates.Lat))
                handles.Candidates.Idx = tmpidx;
                if bWaveIVmoved
                    [~,handles.Candidates.BDPIdx] = min(abs(handles.Candidates.BDPLat-handles.Candidates.Lat(handles.Candidates.Idx(4))));
                end
            end
            guidata(handles.figABRAnalyser,handles);
            PlotWaves(handles);
            FillTable(handles);
        end
    end
    
    
function PlotWaves(handles)
    for k = 1:min([length(handles.Candidates.Idx) length(handles.Candidates.Lat) length(handles.Candidates.Amp)]);
        set(handles.WaveMarker(k),'XData',handles.Candidates.Lat(handles.Candidates.Idx(k)));
        set(handles.WaveMarker(k),'YData',handles.Candidates.Amp(handles.Candidates.Idx(k)));
        
        set(handles.WaveText(k),'Position',[handles.Candidates.Lat(handles.Candidates.Idx(k)) ...
            handles.Candidates.Amp(handles.Candidates.Idx(k))+0.1 ...
            0]);
        if ~isempty(handles.min_markers)
            for m = 1:2:length(handles.min_markers)
                if isvalid(handles.min_markers(m))
                    handles.min_markers(m).set_earlier_than(handles.Candidates.Lat(min(end,handles.Candidates.Idx(4))));
                end
                if isvalid(handles.min_markers(m+1))
                    handles.min_markers(m+1).set_later_than(handles.Candidates.Lat(min(end,handles.Candidates.Idx(4))));
                end
            end
        end
        
    end
    if ~isempty(handles.Candidates.BDPLat)
        set(handles.BDPMarker,'XData',handles.Candidates.BDPLat(handles.Candidates.BDPIdx));
        set(handles.BDPMarker,'YData',handles.Candidates.BDPAmp(handles.Candidates.BDPIdx));
        set(handles.BDPText,'Position',[handles.Candidates.BDPLat(handles.Candidates.BDPIdx) ...
            handles.Candidates.BDPAmp(handles.Candidates.BDPIdx)+0.1 ...
            0]);
        set(handles.BDPMarker,'visible','on');
        set(handles.BDPText,'visible','on');
    else
        set(handles.BDPMarker,'visible','off');
        set(handles.BDPText,'visible','off');
    end
    if ~isempty(handles.Candidates.DPLat)
        for k = 1:min([length(handles.Candidates.DPIdx) length(handles.Candidates.DPLat) length(handles.Candidates.DPAmp)]);
            set(handles.DPMarker(k),'XData',handles.Candidates.DPLat(handles.Candidates.DPIdx(k)));
            set(handles.DPMarker(k),'YData',handles.Candidates.DPAmp(handles.Candidates.DPIdx(k)));
            set(handles.DPText(k),'Position',[handles.Candidates.DPLat(handles.Candidates.DPIdx(k)) ...
                handles.Candidates.DPAmp(handles.Candidates.DPIdx(k))+0.1 ...
                0]);
            if strcmp(get(handles.mnAddDPMark, 'Checked'), 'on')
                set(handles.DPMarker(k),'visible','on');
                set(handles.DPText(k),'visible','on');
            end
        end
    else
        set(handles.DPMarker,'visible','off');
        set(handles.DPText,'visible','off');
    end
    
    
    
    
function FillTable(handles)
    if ~isempty(handles.CurrentRow)
        TD = get(handles.tblData,'Data');
        TD{handles.CurrentRow,6} = handles.Candidates.BDPLat(handles.Candidates.BDPIdx);
        TD{handles.CurrentRow,7} = handles.Candidates.BDPAmp(handles.Candidates.BDPIdx);
        for k = 1:min(handles.NumWaves,length(handles.Candidates.Idx))
            TD{handles.CurrentRow,7+(k-1)*2+1} = handles.Candidates.Lat(handles.Candidates.Idx(k));
            TD{handles.CurrentRow,7+(k-1)*2+2} = handles.Candidates.Amp(handles.Candidates.Idx(k));
        end
        handles.Table{handles.LastIndex,2}{handles.CurrentRow,1} = handles.Candidates.Lat;
        handles.Table{handles.LastIndex,2}{handles.CurrentRow,2} = handles.Candidates.Amp;
        handles.Table{handles.LastIndex,2}{handles.CurrentRow,3} = handles.Candidates.Idx;
        handles.Table{handles.LastIndex,2}{handles.CurrentRow,4} = handles.Candidates.BDPLat;
        handles.Table{handles.LastIndex,2}{handles.CurrentRow,5} = handles.Candidates.BDPAmp;
        handles.Table{handles.LastIndex,2}{handles.CurrentRow,6} = handles.Candidates.BDPIdx;
        offset = 2*min(handles.NumWaves,length(handles.Candidates.Idx));
        for k = 1:min(handles.NumDPs,min(length(handles.Candidates.DPIdx), length(handles.Candidates.DPLat)))
            TD{handles.CurrentRow,7+offset+(k-1)*2+1} = handles.Candidates.DPLat(handles.Candidates.DPIdx(k));
            TD{handles.CurrentRow,7+offset+(k-1)*2+2} = handles.Candidates.DPAmp(handles.Candidates.DPIdx(k));
        end
        set(handles.tblData,'Data',TD);
        restoreView(handles);
    end

    

function restoreView(handles)
    jscrollpane = javaObjectEDT(findjobj(handles.tblData));
    viewport    = javaObjectEDT(jscrollpane.getViewport);
    jtable      = javaObjectEDT( viewport.getView );
    jtable.scrollRowToVisible(handles.CurrentRow + 2);


% --------------------------------------------------------------------
function mnSettimelimits_Callback(hObject, eventdata, handles)
    answer = inputdlg({'Minimal time limit:', 'Maximal time limit:'}, ...
                       'Time Limits', 1, ...
                       {num2str(handles.Settings.TimeLimits(1)), ...
                        num2str(handles.Settings.TimeLimits(2))});
    if ~isempty(answer)
        limits = cellfun(@str2num,answer);
        limits(isnan(limits) | isinf(limits)) = 0;
        limits = unique(limits);
        if length(limits) ~= 2
            limits = handles.Settings.TimeLimits;
        end
        handles.Settings.TimeLimits = limits;
        xlim(handles.axWaveForm, handles.Settings.TimeLimits);    
        saveSettings(handles)
        guidata(hObject, handles);
    end


% --------------------------------------------------------------------
function mnSetFilterLimits_Callback(hObject, eventdata, handles)
    answer = inputdlg({'Low frequency limit:', 'High frequency limit:'}, ...
                       'Filter Limits', 1, ...
                       {num2str(handles.Settings.FilterCutOff(1)), ...
                        num2str(handles.Settings.FilterCutOff(2))});
    if ~isempty(answer)
        limits = cellfun(@str2num,answer);
        limits(isnan(limits) | isinf(limits)) = 300;
        limits = unique(limits);
        if length(limits) ~= 2
            limits = handles.Settings.FilterCutOff;
        end
        handles.Settings.FilterCutOff = limits;
        saveSettings(handles)
        guidata(hObject, handles);
    end
