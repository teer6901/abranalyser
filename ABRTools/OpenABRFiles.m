function [file_list, data_table, path_name] = OpenABRFiles(path_name)
    [file_list, path_name] = uigetfile({'*.mat','data files';'*.m','file list'}, ...
                                        'Select Files',[path_name '/'],...
                                        'MultiSelect','on');
    
    if iscellstr(file_list) || ischar(file_list)
        
        if ~isempty(file_list)
            data_table     = {[],[]};
            if ischar(file_list) == 1 && strcmp(file_list(end-1:end),'.m')
                tmppath = pwd;
                cd(path_name);
                eval(file_list(1:end-2));
                cd(tmppath);
                path_name = PathName;
                c = 1;
                for k = 1:length(FileNames)
                    for m = 1:length(FileNames{k})
                        tmp{c} = FileNames{k}{m};
                        c = c + 1;
                    end
                end
                file_list = tmp;
            elseif ischar(file_list) == 1 && strcmp(file_list(end-3:end),'.mat')
                tmp = who('-file',fullfile(path_name,file_list));
                if any(strcmp(tmp,'Table')) && any(strcmp(tmp,'Files')) && any(strcmp(tmp,'Path'))
                    tmp = load(fullfile(path_name,file_list));
                    data_table = tmp.Table;
                    file_list = tmp.Files;
                else
                    data_table = {};
                    file_list = cellstr(file_list);
                end
            end
        end
        
    end
