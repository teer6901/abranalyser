function ABRParameterList(file_list, path_name)
    
    LT_tag = {'ITD/ILD', 'monaural level threshold'};
    true_false = {'no', 'yes'};
    
    for ix = 1:length(file_list)
        if iscell(file_list{ix})
            n_sub_files = length(file_list{ix});
        else
            n_sub_files = 1;
        end
        for fx = 1:n_sub_files
            
            fprintf('--------------------------------------------------\n');
            
            if iscell(file_list{ix})
                dataset = load(fullfile(path_name,file_list{ix}{fx}));
                fprintf('%s\n', file_list{ix}{fx});
            else
                dataset = load(fullfile(path_name,file_list{ix}));
                fprintf('%s\n', file_list{ix});
            end
            
            if isfield(dataset.St, 'PresentationType')
                fprintf('  %s, ', dataset.St.PresentationType);
                if isfield(dataset.St, 'StimulusSide')
                    fprintf('Stim: %s ', dataset.St.StimulusSide);
                end
                if isfield(dataset.St, 'MaskerSide')
                    fprintf('Masker: %s ', dataset.St.MaskerSide);
                end
                fprintf('\n');
            end
            
            fprintf('  %s, %s, fs: %1.0f\n', LT_tag{dataset.St.LevelThreshold+1}, dataset.St.Type, dataset.St.Fs);
            if ~dataset.St.LevelThreshold
                fprintf('  ITD µs: ');
                for k = 1:length(dataset.St.ITD)
                    fprintf('%1.1f ', dataset.St.ITD(k)/1e-6);
                end
                fprintf('\n');
            end
            if ~dataset.St.LevelThreshold
                fprintf('  ILD dB: ');
            else
                fprintf('  Level dB: ');
            end
            for k = 1:length(dataset.St.ILD)
                fprintf('%1.1f ', dataset.St.ILD(k));
            end
            fprintf('\n');
            if ~dataset.St.LevelThreshold
                fprintf('  Level dB: %1.1f', dataset.St.Level);
                fprintf('\n');
            end
            
            fprintf('  Reps: %1.0f - %1.0f\n', min(dataset.AvgC(:)), max(dataset.AvgC(:)));
            
            switch dataset.St.Type
                case 'tone'
                    fprintf('  Freq: %1.1f,', dataset.St.Frequency);
                    fprintf('  Dur.: %1.1f ms\n', dataset.St.Duration/1e-3);
                case 'click'
                case 'standardclick'
                case 'doubleclick'
                case 'broadband noise'
                    fprintf('  Dur.: %1.1f ms\n', dataset.St.Duration/1e-3);
                case 'narrowband noise'
                    fprintf('  Dur.: %1.1f ms\n', dataset.St.Duration/1e-3);
                case 'modulated noise'
                    fprintf('  Freq: %1.1f,', dataset.St.Frequency);
                    fprintf('  Mod-Freq.: %1.1f,', dataset.St.ModulationDepth);
                    fprintf('  Dur.: %1.1f ms\n', dataset.St.Duration/1e-3);
                case {'wave','raw'}
                    fprintf('  File: %s,', dataset.St.FileName);
%                     fprintf('  %s,', dataset.St.SampleFormat);
%                     fprintf('  %1.0f, ', dataset.St.FileCh);
%                     fprintf('  %s,', true_false{dataset.St.bDoResample+1});
%                     fprintf('  %1.0f\n', dataset.St.FileTimeOffset);
                case 'CAP'
                    fprintf('  %1.0f ', dataset.St.Frequency);
                    fprintf('  %1.0f ', dataset.St.BufferLen);
                    fprintf('  %1.0f ', dataset.St.IAC);
                    fprintf('  %1.0f ', dataset.St.CenterFreq);
                    fprintf('  %1.0f ', dataset.St.BandWidth);
                    fprintf('  %1.0f ', dataset.St.MaskerLevel);
                    fprintf('  %1.0f ', dataset.St.MaskerDuration);
                    fprintf('  %1.0f ', dataset.St.StimOnsetDelay);
                    fprintf('  %1.0f ', dataset.St.MaskerLevelOffsets);
                    fprintf('  %1.0f ', dataset.St.StimulusLevelOffsets);
                    fprintf('  %1.0f ', dataset.St.MaskerRampDur);
                    fprintf('  %1.0f ', dataset.St.MaskerFrozen);
                    fprintf('  %1.0f ', dataset.St.BufferLen);
                otherwise
            end
            
            switch dataset.St.Window
                case 'none'
                otherwise
                    fprintf('  ramp dur: %1.1f ms, type: %s\n', dataset.St.RampDur/1e-3, dataset.St.Window);
            end
            
            
            optional_parameters = { ...
                % parameters ...
                false, '  %1.1f ', 'Hw', 'PhysAmp_GainFactor', 1 ; ...
                false, '  %1.1f ', 'Hw', 'SoundCard_In_Impedance', 1 ; ...
                false, '  %1.1f ', 'Hw', 'PhysAmp_Out_Impedance', 1 ; ...
                false, '  %1.1f ', 'Hw', 'SoundCardVoltToSample', 1 ; ...
                false, '  %1.1f ', 'Hw', 'Mic_Cal_Value', 1 ; ...
                false, '  %1.1f ', 'Hw', 'Mic_PascalToVolt', 1 ; ...
                false, '  %1.1f ', 'Hw', 'MicAmp_In_Impedance', 1 ; ...
                false, '  %1.1f ', 'Hw', 'Mic_Out_Impedance', 1 ; ...
                false, '  %1.1f ', 'Hw', 'MicAmp_GainFactor', 1 ; ...
                false, '  %1.1f ', 'Hw', 'SoundCard_In_Impedance', 1 ; ...
                false, '  %1.1f ', 'Hw', 'MicAmp_Out_Impedance', 1 ; ...
                false, '  %1.1f ', 'Hw', 'SoundCardVoltToSample', 1 ; ...
                false, '  %1.1f ', 'Hw', 'Mic_Cal_Value', 1 ; ...
                %% sound device initialization
                false, '  %s '   , 'Hw', 'DryRun', true_false ; ...
                false, '  %1.1f ', 'Hw', 'PlayDev', 1 ; ...
                false, '  %1.1f ', 'Hw', 'RecDev', 1 ; ...
                false, '  %1.1f ', 'Hw', 'PlayCh', 1 ; ...
                false, '  %1.1f ', 'Hw', 'RecCh', 1 ; ...
                false, '  %1.1f ', 'Hw', 'BufferSize', 1 ; ...
                false, '  %1.1f ', 'Hw', 'StimCh', 1 ; ...
                false, '  %1.1f ', 'Hw', 'TrgCh', 1 ; ...
                %% preparation
                true, '  Rectime: %1.1f ms,', 'Rc', 'RecTime', 1e-3 ; ...
                true, '  Pretime: %1.1f ms,', 'Rc', 'PreTime', 1e-3 ; ...
                true, '  Extrasmp: %1.0f', 'Rc', 'ExtraSmp', 1 ; ...
                true, '  (%1.1f ms),', 'Rc', 'ExtraSmp', dataset.St.Fs*1e-3 ; ...
                false, '  %1.1f ', 'Rc', 'MicCh', 1 ; ...
                false, '  %1.1f ', 'Rc', 'EEGCh', 1 ; ...
                false, '  %1.1f ', 'Rc', 'MaxRepsPerCond', 1 ; ...
                false, '  %s '   , 'Rc', 'FileName', [] ; ...
                false, '  %s '   , 'Rc', 'RejectArtefacts', true_false; ...
                true, '  Artefact-Thr.: %1.1f µV\n', 'Rc', 'ArtefactThr', 1 ; ...
                %% stimulus time delay
                false, '  %1.1f ', 'St', 'StimOnsetDelay', 1 ; ...
                %% load calibration data
                false, '  %s ', 'Hw', 'CalFile', [] ; ...
                false, '  %1.1f ', 'Hw', 'LevelCorrection', 1 };
            
            for k = 1:size(optional_parameters, 1)
                if optional_parameters{k, 1}
                    if isfield(dataset, optional_parameters{k, 3})
                        tmp = dataset.(optional_parameters{k, 3});
                        if isfield(tmp, optional_parameters{k, 4})
                            if iscell(optional_parameters{k, 5})
                                fprintf(optional_parameters{k, 2}, optional_parameters{k, 5}{tmp.(optional_parameters{k, 4})+1});
                            elseif isempty(optional_parameters{k, 5})
                                fprintf(optional_parameters{k, 2}, tmp.(optional_parameters{k, 4}));
                            else
                                fprintf(optional_parameters{k, 2}, tmp.(optional_parameters{k, 4})./optional_parameters{k, 5});
                            end
                        end
                    end
                end
            end         
                
            
        end
    end
