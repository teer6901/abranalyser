function data = LoadABRFilesLinear(file_list, file_index, path_name)
    
    %% merge data files
    Avg  = [];
    AvgC = [];
    Mic  = [];
    MicC = [];
    ITD = [];
    ILD = [];
    Fs = [];
    Level = [];
    Subject = [];
    Side = [];
    
    for ix = 1:length(file_index)
        if iscell(file_list{file_index(ix)})
            n_sub_files = length(file_list{file_index(ix)});
        else
            n_sub_files = 1;
        end
        for fx = 1:n_sub_files
            
            if iscell(file_list{file_index(ix)})
                tmp = load(fullfile(path_name, file_list{file_index(ix)}{fx}));
            else
                tmp = load(fullfile(path_name, file_list{file_index(ix)}));
            end
            
            if tmp.St.Fs  ==  96000
                answer = questdlg('The sampling rate saved in the file is 96000 Hz. This might be wrong. Do you want to correct it to 48000 Hz?', ...
                    'Sampling rate wrong', ...
                    'Yes (48000 Hz)', 'No (96000 Hz)', 'Yes (48000 Hz)');
                if strcmp(answer, 'Yes (48000 Hz)')
                    tmp.St.Fs = 48000;
                end
            end
            
            if ~isempty(Fs) && ~all(Fs  ==  tmp.St.Fs)
                error('experiments do not match: sampling rate');
            else
                Fs = tmp.St.Fs;
            end
            
            Avg  = cat(2, Avg,  reshape(tmp.Avg,  size(tmp.Avg,  1), []));
            AvgC = cat(1, AvgC, reshape(tmp.AvgC, [], 1));
            Mic  = cat(3, Mic,  reshape(tmp.Mic(:, 1:2, :, :),  size(tmp.Mic,  1), 2, []));
            MicC = cat(1, MicC, reshape(tmp.MicC, [], 1));
            
            [ITD_, ILD_, Level_, Subject_] = ndgrid([0;0;tmp.St.ITD(:)], tmp.St.ILD(:), tmp.St.Level, ix);
            
            if all(Level_(:) == 0) && (numel(unique(ILD_)) > 1)
                Level_ = ILD_;
                ILD_(:) = 0;
            end
            
            ITD     = cat(1, ITD, ITD_(:));
            ILD     = cat(1, ILD, ILD_(:));
            Level   = cat(1, Level, Level_(:));
            Subject = cat(1, Subject, Subject_(:));

            [Side_, ~, ~, ~] = ndgrid([1;2;3*ones(size(tmp.St.ITD(:)))], tmp.St.ILD(:), tmp.St.Level, ix);
            Side = cat(1, Side, Side_(:));
            
        end
    end
    
    [~, order] = sortrows([Level Side ITD ILD Subject]);
    
    Avg  =  Avg(:, order);
    AvgC = AvgC(order);
    Mic  =  Mic(:, order);
    MicC = MicC(order);
    
    Level   =   Level(order);
    ITD     =     ITD(order);
    ILD     =     ILD(order);
    Side    =    Side(order);
    Subject = Subject(order);

    data = struct('Avg', Avg, 'Mic', Mic, 'Fs', Fs, 'ITD', ITD, 'ILD', ILD, ...
                  'Level', Level, 'Side', Side,  'Subject', Subject, 'AvgC', AvgC, ...
                  'MicC', MicC, 'Rc', tmp.Rc);
    %%
    
