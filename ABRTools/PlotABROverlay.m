function PlotABROverlay(data, selection, levels, ilds, itds, spread, polarity, shift_itd, new_figure)

    %% default values
    selection = cellstr(selection);
    plot_left = true;
    plot_right = true;
    latency_offset = -0.351e-3;
    
    %% plot replacement
    if new_figure
        figure
    end
    
    D = polarity * data.Avg;
    D(isinf(D)|isnan(D)) = 0;
    
    %% filtering
    b = fir1(100, [50 3000]/data.Fs*2); a = 1;
    data_size = size(D);
    D = reshape(fftfilt(b, reshape(D, data_size(1), [])), data_size);
    gd = mean(grpdelay(b, a, linspace(50, 3000, 101), data.Fs)); % adjust for filter group delay

%     D = squeeze(data.Mic(:, 1, :, :, :)); gd = 0;
    t = (((0:size(D, 1)-1) - gd).'/data.Fs - data.Rc.PreTime + latency_offset)/1e-3;

    if data.St.LevelThreshold || all(data.Level == 0)
        data.Level = data.ILD;
        data.ILD = 0;
        D = permute(D, [1 2 4 3]);
    end

    if ~isempty(itds)
        itd_idx = ismember([NaN; NaN; data.ITD], itds);
    else
        itd_idx = true(size(D, 2),1);
        itd_idx(1) = false;
        itd_idx(2) = false;
    end
    
    if ~isempty(ilds)
        ild_idx = ismember(data.ILD, ilds);
    else
        ild_idx = true(size(D, 3),1);
    end
    
    if ~isempty(levels)
        level_idx = ismember(data.Level, levels);
    else
        level_idx = true(size(D, 4), 1);
    end

    if ~isempty(selection)
        if ~any(strcmpi(selection, 'B'))
            itd_idx = false(size(D, 2), 1);
        end
        if any(strcmpi(selection, 'L'))
            plot_left = true;
        else
            plot_left = false;
        end
        if any(strcmpi(selection, 'R'))
            plot_right = true;
        else
            plot_right = false;
        end
    end

    
    shift = 0:((sum(itd_idx)+plot_left+plot_right)*sum(ild_idx)*sum(level_idx))-1;
    shift = reshape(shift, 1, sum(itd_idx)+plot_left+plot_right, sum(ild_idx), sum(level_idx));
    shift = spread * shift;

    clf
    if plot_left
        plot(t, reshape(bsxfun(@plus, D(:, 1, ild_idx, level_idx), shift(:, 1, :, :)), size(D, 1), []), 'b-');
        hold on
    end
    if plot_right
        plot(t, reshape(bsxfun(@plus, D(:, 2, ild_idx, level_idx), shift(:, plot_left+1, :, :)), size(D, 1), []), 'r-');
        hold on
    end
    if any(itd_idx) && any(ild_idx) && any(level_idx)
        itd_idx = find(itd_idx);
        for ix = 1:length(itd_idx)
            plot(t + shift_itd * abs(data.ITD(itd_idx(ix)-2)/2/1e-3), reshape(bsxfun(@plus, D(:, itd_idx(ix), ild_idx, level_idx), shift(:, plot_left+plot_right+ix, :, :)), size(D, 1), []), 'k-');
            hold on
        end
    end
    hold off
    grid on
    xlim([-1 9]);
    ylim([-1.1 1.1] * max(abs(D(:))) + [0 max(shift(:))]);
    
    line(get(gca, 'xlim'), [0;0], 'linestyle', '--', 'color', 'k');
    line([0;0], get(gca, 'ylim'), 'linestyle', '-', 'color', 'k')
    
    set(gca, 'xtick', -1:9);
    if spread <= 0
       set(gca, 'ytick', -20:20, 'yticklabel', -20:20);
    else
       if sum(level_idx) > 1
           labels = repmat(data.Level(level_idx), length(itd_idx)+plot_left+plot_right, sum(ild_idx), 1);
       elseif sum(itd_idx) > 1
           labels = repmat([0; 0; data.ITD(itd_idx-2)]/1e-6, 1, sum(ild_idx), sum(level_idx));
       else
           labels = repmat(data.ILD(ild_idx), length(itd_idx)+plot_left+plot_right, 1, sum(level_idx));
       end
       set(gca, 'ytick', unique(shift(:)), 'yticklabel', labels);
    end
    
    

    
    
    
