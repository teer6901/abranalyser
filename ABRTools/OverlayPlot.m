function varargout = OverlayPlot(varargin)
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
        'gui_Singleton',  gui_Singleton, ...
        'gui_OpeningFcn', @OverlayPlot_OpeningFcn, ...
        'gui_OutputFcn',  @OverlayPlot_OutputFcn, ...
        'gui_LayoutFcn',  [] , ...
        'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end
    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    
function OverlayPlot_OpeningFcn(hObject, eventdata, handles, varargin)
    handles.current.file_list = {};
    handles.current.datatable = {};
    if length(varargin) >= 1 && ischar(varargin{1})
        handles.current.folder = varargin{1};
    else
        handles.current.folder = '.';
    end
    handles.current.data = [];
    handles.current.figure = 1;
    handles.output = hObject;
    guidata(hObject, handles);
    
    
function varargout = OverlayPlot_OutputFcn(hObject, eventdata, handles)
    varargout{1} = handles.output;
    
    %%
function files_Callback(hObject, eventdata, handles)
    index = get(hObject, 'Value');
    handles.current.data = LoadABRFilesLinear(handles.current.file_list, index, handles.current.folder);
    fill_listboxes(handles);
    guidata(hObject, handles);
    
function side_Callback(hObject, eventdata, handles)
    
    
function itds_Callback(hObject, eventdata, handles)
    
    
function ilds_Callback(hObject, eventdata, handles)
    
    
function levels_Callback(hObject, eventdata, handles)
    
    
function draw_Callback(hObject, eventdata, handles)
    if isstruct(handles.current.data)
        side_idx = get(handles.side, 'Value');
        sides = get(handles.side, 'String');
        level_idx = get(handles.levels, 'Value');
        levels = str2double(get(handles.levels, 'String'));
        itd_idx = get(handles.itds, 'Value');
        itds = str2double(get(handles.itds, 'String')) * 1e-6;
        ild_idx = get(handles.ilds, 'Value');
        ilds = str2double(get(handles.ilds, 'String'));

        if ishandle(handles.current.figure)
            figure(handles.current.figure);
        else
            handles.current.figure = figure;
        end
        PlotABROverlayLinear(handles.current.data , sides(side_idx), ...
                        levels(level_idx), ...
                        ilds(ild_idx), ...
                        itds(itd_idx), ...
                        get(handles.spread, 'Value'), ...
                        (-1)^get(handles.polarity, 'Value'), ...
                        get(handles.shiftitd, 'Value') == 1, ...
                        false, ...
                        get(handles.colorchoice, 'Value') == 1);
        figure(handles.overlayplot);
    end
    
function newfig_Callback(hObject, eventdata, handles)
    handles.current.figure = figure;
    guidata(hObject, handles);
    
function spread_Callback(hObject, eventdata, handles)
    draw_Callback(handles.draw, eventdata, handles);
    
function polarity_Callback(hObject, eventdata, handles)
    draw_Callback(handles.draw, eventdata, handles);
    
function colorchoice_Callback(hObject, eventdata, handles)
    
    
function openfile_Callback(hObject, eventdata, handles)
    [handles.current.file_list, ...
        handles.current.datatable, ...
        handles.current.folder] = OpenABRFiles(handles.current.folder);
    if ~isempty(handles.current.file_list)
        set(handles.files, 'String', handles.current.file_list);
        set(handles.files, 'Value', []);
        set(handles.side, 'Value', []);
        set([handles.side, handles.itds, handles.levels, handles.draw, ...
             handles.spread, handles.ilds, handles.files, handles. polarity, ...
             handles.paramlist, handles.shiftitd, handles.colorchoice], 'Enable', 'on');
    end
    guidata(hObject, handles);
    

function paramlist_Callback(hObject, eventdata, handles)
    index = get(handles.files, 'Value');
    if isempty(index)
        index = 1:length(handles.current.file_list);
    end
    ABRParameterList(handles.current.file_list(index), handles.current.folder);
    
    
function shiftitd_Callback(hObject, eventdata, handles)
    
    
function fill_listboxes(handles)
    if isstruct(handles.current.data) && isfield(handles.current.data, 'Level')
        set(handles.levels, 'String', strtrim(cellstr(num2str(unique(handles.current.data.Level)))));
        set(handles.levels, 'Value', []);
    end
    if isstruct(handles.current.data) && isfield(handles.current.data, 'ITD')
        set(handles.itds, 'String', strtrim(cellstr(num2str(unique(handles.current.data.ITD/1e-6)))));
        set(handles.itds, 'Value', []);
    end
    if isstruct(handles.current.data) && isfield(handles.current.data, 'ILD')
        set(handles.ilds, 'String', strtrim(cellstr(num2str(unique(handles.current.data.ILD)))));
        set(handles.ilds, 'Value', []);
    end
    
    
    %%
function files_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
function spread_CreateFcn(hObject, eventdata, handles)
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end
function levels_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
function ilds_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
function itds_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
function side_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


