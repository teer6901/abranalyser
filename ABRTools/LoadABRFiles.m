function data = LoadABRFiles(file_list, file_index, path_name)
    
    %% merge data files
    Avg  = 0;
    AvgC = 0;
    Mic  = 0;
    MicC = 0;
    ITD = [];
    ILD = [];
    Fs = [];
    Level = [];
    
    for ix = 1:length(file_index)
        if iscell(file_list{file_index(ix)})
            n_sub_files = length(file_list{file_index(ix)});
        else
            n_sub_files = 1;
        end
        for fx = 1:n_sub_files
            
            if iscell(file_list{file_index(ix)})
                tmp = load(fullfile(path_name, file_list{file_index(ix)}{fx}));
            else
                tmp = load(fullfile(path_name, file_list{file_index(ix)}));
            end
            
            [tmp.St.ITD, ITDorder] = sort(tmp.St.ITD);
            [tmp.St.ILD, ILDorder] = sort(tmp.St.ILD);
            
            tmp.Avg  = tmp.Avg(:, [1 2 ITDorder+2], :);
            tmp.Avg  = tmp.Avg(:, :, ILDorder);
            
            tmp.AvgC = tmp.AvgC([1 2 ITDorder+2], :);
            tmp.AvgC = tmp.AvgC(:, ILDorder);
            
            tmp.Mic  = tmp.Mic(:, :, [1 2 ITDorder+2], :);
            tmp.Mic  = tmp.Mic(:, :, :, ILDorder);
            
            tmp.MicC = tmp.MicC([1 2 ITDorder+2], :);
            tmp.MicC = tmp.MicC(:, ILDorder);
            
            if tmp.St.Fs  ==  96000
                answer = questdlg('The sampling rate saved in the file is 96000 Hz. This might be wrong. Do you want to correct it to 48000 Hz?', ...
                    'Sampling rate wrong', ...
                    'Yes (48000 Hz)', 'No (96000 Hz)', 'Yes (48000 Hz)');
                if strcmp(answer, 'Yes (48000 Hz)')
                    tmp.St.Fs = 48000;
                end
            end
            
            if ~isempty(Fs) && ~all(Fs  ==  tmp.St.Fs)
                error('experiments do not match: sampling rate');
            else
                Fs = tmp.St.Fs;
            end
            
            if isempty(ITD) && isempty(ILD)
                ITD = tmp.St.ITD(:);
                ILD = tmp.St.ILD(:);
                Level = tmp.St.Level(:);
                Avg  = zeros(size(tmp.Avg, 1),                   2+length(ITD), length(ILD), length(Level));
                AvgC = zeros(                                    2+length(ITD), length(ILD), length(Level));
                Mic  = zeros(size(tmp.Mic, 1), size(tmp.Mic, 2), 2+length(ITD), length(ILD), length(Level));
                MicC = zeros(                                    2+length(ITD), length(ILD), length(Level));
            end
            
            for lx = 1:length(tmp.St.Level)
                % insert new Level dimension if necessary
                if ~ismember(tmp.St.Level(lx), Level)
                    Level = cat(1, Level, tmp.St.Level(lx));
                     Avg = cat(4,  Avg(:,    :, :, :), zeros(size(Avg, 1),               size(Avg , 2), size(Avg,  3), 1));
                    AvgC = cat(3, AvgC(      :, :, :), zeros(                            size(AvgC, 1), size(AvgC, 2), 1));
                     Mic = cat(5,  Mic(:, :, :, :, :), zeros(size(Mic, 1), size(Mic, 2), size(Mic , 3), size(Mic,  4), 1));
                    MicC = cat(3, MicC(      :, :, :), zeros(                            size(MicC, 1), size(MicC, 2), 1));
                end
                
                lpos = find(Level == tmp.St.Level(lx)); t_lpos = find(tmp.St.Level == tmp.St.Level(lx));

                for ilx = 1:length(tmp.St.ILD)
                    % insert new ILD column if necessary
                    if ~ismember(tmp.St.ILD(ilx), ILD)
                        ILD  = cat(1, ILD, tmp.St.ILD(ilx));
                         Avg = cat(3,  Avg(:,    :, :, :), zeros(size(Avg, 1),               size(Avg , 2), 1, size(Avg,  4)));
                        AvgC = cat(2, AvgC(      :, :, :), zeros(                            size(AvgC, 1), 1, size(AvgC, 3)));
                         Mic = cat(4,  Mic(:, :, :, :, :), zeros(size(Mic, 1), size(Mic, 2), size(Mic , 3), 1, size(Mic,  5)));
                        MicC = cat(2, MicC(      :, :, :), zeros(                            size(MicC, 1), 1, size(MicC, 3)));
                    end
                    ilpos = find(ILD == tmp.St.ILD(ilx));   t_ilpos = find(tmp.St.ILD == tmp.St.ILD(ilx));

                    % left channel (index 1)
                    itpos = 1;                              t_itpos = 1;
                     Avg(:,    itpos, ilpos, lpos) =  Avg(:,    itpos, ilpos, lpos) + tmp.AvgC(t_itpos, t_ilpos, t_lpos) .* tmp.Avg(:,    t_itpos, t_ilpos, t_lpos);
                    AvgC(      itpos, ilpos, lpos) = AvgC(      itpos, ilpos, lpos) + tmp.AvgC(t_itpos, t_ilpos, t_lpos);
                     Mic(:, :, itpos, ilpos, lpos) =  Mic(:, :, itpos, ilpos, lpos) + tmp.MicC(t_itpos, t_ilpos, t_lpos) .* tmp.Mic(:, :, t_itpos, t_ilpos, t_lpos);
                    MicC(      itpos, ilpos, lpos) = MicC(      itpos, ilpos, lpos) + tmp.MicC(t_itpos, t_ilpos, t_lpos);

                    % right channel (index 2)
                    itpos = 2;                              t_itpos = 2;
                     Avg(:,    itpos, ilpos, lpos) =  Avg(:,    itpos, ilpos, lpos) + tmp.AvgC(t_itpos, t_ilpos, t_lpos) .* tmp.Avg(:,    t_itpos, t_ilpos, t_lpos);
                    AvgC(      itpos, ilpos, lpos) = AvgC(      itpos, ilpos, lpos) + tmp.AvgC(t_itpos, t_ilpos, t_lpos);
                     Mic(:, :, itpos, ilpos, lpos) =  Mic(:, :, itpos, ilpos, lpos) + tmp.MicC(t_itpos, t_ilpos, t_lpos) .* tmp.Mic(:, :, t_itpos, t_ilpos, t_lpos);
                    MicC(      itpos, ilpos, lpos) = MicC(      itpos, ilpos, lpos) + tmp.MicC(t_itpos, t_ilpos, t_lpos);

                    for tx = 1:length(tmp.St.ITD)

                        % insert new ITD column if necessary
                        if ~ismember(tmp.St.ITD(tx), ITD)
                            ITD  = cat(1, ITD, tmp.St.ITD(tx));
                             Avg = cat(2,  Avg(:,    :, :, :), zeros(size(Avg, 1),               1, size(Avg , 3), size(Avg,  4)));
                            AvgC = cat(1, AvgC(      :, :, :), zeros(                            1, size(AvgC, 2), size(AvgC, 3)));
                             Mic = cat(3,  Mic(:, :, :, :, :), zeros(size(Mic, 1), size(Mic, 2), 1, size(Mic , 4), size(Mic , 5)));
                            MicC = cat(1, MicC(      :, :, :), zeros(                            1, size(MicC, 2), size(MicC, 3)));
                        end

                        % position in summary Avg etc     % position in tmp.Avg etc.
                        itpos = 2+find(ITD == tmp.St.ITD(tx));   t_itpos = 2+find(tmp.St.ITD == tmp.St.ITD(tx));
                        
                         Avg(:,    itpos, ilpos, lpos) =  Avg(:,    itpos, ilpos, lpos) ...
                             + bsxfun(@times, permute(tmp.AvgC(t_itpos, t_ilpos, t_lpos), [4 1 2 3]), tmp.Avg(:,    t_itpos, t_ilpos, t_lpos));
                        AvgC(      itpos, ilpos, lpos) = AvgC(      itpos, ilpos, lpos) + tmp.AvgC(t_itpos, t_ilpos, t_lpos);
                        
                         Mic(:, :, itpos, ilpos, lpos) =  Mic(:, :, itpos, ilpos, lpos) ...
                             + bsxfun(@times, permute(tmp.MicC(t_itpos, t_ilpos, t_lpos), [4 5 1 2 3]), tmp.Mic(:, :, t_itpos, t_ilpos, t_lpos));
                        MicC(      itpos, ilpos, lpos) = MicC(      itpos, ilpos, lpos) + tmp.MicC(t_itpos, t_ilpos, t_lpos);

                    end
                end
            end
            
        end
    end
    
    Avg = bsxfun(@rdivide, Avg, permute(AvgC, [4 1 2 3]));
    Mic = bsxfun(@rdivide, Mic, permute(MicC, [4 5 1 2 3]));
    
    [ITD, order] = sort(ITD); order = [1; 2; order + 2];
     Avg =  Avg(:,    order, :, :);
    AvgC = AvgC(      order, :, :);
     Mic =  Mic(:, :, order, :, :);
    MicC = MicC(      order, :, :);
    
    [ILD, order] = sort(ILD);
     Avg =  Avg(:,    :, order, :);
    AvgC = AvgC(      :, order, :);
     Mic =  Mic(:, :, :, order, :);
    MicC = MicC(      :, order, :);
    
    [Level, order] = sort(Level);
     Avg =  Avg(:,    :, :, order);
    AvgC = AvgC(      :, :, order);
     Mic =  Mic(:, :, :, :, order);
    MicC = MicC(      :, :, order);
    
    data = struct('Avg', Avg, 'Mic', Mic, 'Fs', Fs, 'ITD', ITD, 'ILD', ILD, ...
                  'Level', Level, 'AvgC', AvgC, 'MicC', MicC, 'St', tmp.St, ...
                  'Rc', tmp.Rc, 'Hw', tmp.Hw);
    %%
    
