d = dir('~/projects/AG/ABR/data (Jenny)/*.mat');

for k = 1:length(d)
    load(fullfile('~/projects/AG/ABR/data (Jenny)', d(k).name));

    [~,p]=max(abs(hilbert(reshape(Mic(:,1:2,[1 2 find(St.ITD==0)+2]), 912, []))));
    lat(k,:)=(p(setdiff(1:end,[2 3]))-Rc.PreTime*St.Fs)/St.Fs/1e-3;
end
