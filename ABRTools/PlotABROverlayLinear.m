function PlotABROverlayLinear(data, selection, levels, ilds, itds, spread, polarity, shift_itd, new_figure, random_colors)

    %% default values
    selection = cellstr(selection);
    plot_left = true;
    plot_right = true;
    latency_offset = -0.351e-3;
    
    %% plot replacement
    if new_figure
        figure
    end
    
    D = polarity * data.Avg;
    D(isinf(D)|isnan(D)) = 0;
    
    %% filtering
    b = fir1(100, [50 3000]/data.Fs*2); a = 1;
    data_size = size(D);
    D = reshape(fftfilt(b, reshape(D, data_size(1), [])), data_size);
    gd = mean(grpdelay(b, a, linspace(50, 3000, 101), data.Fs));  % adjust for filter group delay

%     D = squeeze(data.Mic(:, 1, :, :, :)); gd = 0;
    t = (((0:size(D, 1)-1) - gd).'/data.Fs - data.Rc.PreTime + latency_offset)/1e-3;

    if ~isempty(itds)
        itd_idx = ismember(data.ITD, itds);
    else
        itd_idx = data.Side == 3;
    end
    
    if ~isempty(ilds)
        ild_idx = ismember(data.ILD, ilds);
    else
        ild_idx = true(size(D, 2),1);
    end
    
    if ~isempty(levels)
        level_idx = ismember(data.Level, levels);
    else
        level_idx = true(size(D, 2), 1);
    end

    if ~isempty(selection)
        if ~any(strcmpi(selection, 'B'))
            itd_idx = false(size(D, 2), 1);
        end
        if any(strcmpi(selection, 'L'))
            plot_left = true;
        else
            plot_left = false;
        end
        if any(strcmpi(selection, 'R'))
            plot_right = true;
        else
            plot_right = false;
        end
    end
    
    overall_idx = itd_idx & ild_idx & level_idx & data.Side == 3;

    if plot_left
        overall_idx = overall_idx | data.Side == 1;
    end
    if plot_right
        overall_idx = overall_idx | data.Side == 2;
    end
    shift = spread * (cumsum(overall_idx) - 1).';
    shift(shift<0) = 0;

    clf
    if plot_left
        if random_colors
            color = rand(1,3);
        else
            color = 'b-';
        end
        plot(t, bsxfun(@plus, D(:, data.Side==1 & overall_idx), ...
                             shift(data.Side==1 & overall_idx)), 'color', color, 'linestyle', '-');
        hold on
    end
    if plot_right
        if random_colors
            color = rand(1,3);
        else
            color = 'r-';
        end
        plot(t, bsxfun(@plus, D(:, data.Side==2 & overall_idx), ...
                             shift(data.Side==2 & overall_idx)), 'color', color, 'linestyle', '-');
        hold on
    end
    if any(itd_idx) && any(ild_idx) && any(level_idx)
        itd_pos = find(itd_idx);
        for ix = 1:length(itd_pos)
            if random_colors
                color = rand(1,3);
            else
                color = 'k-';
            end
            plot(t + shift_itd * abs(data.ITD(itd_pos(ix))/2/1e-3), ...
                bsxfun(@plus, D(:, data.Side==3 & data.ITD==data.ITD(itd_pos(ix)) & overall_idx), ...
                             shift(data.Side==3 & data.ITD==data.ITD(itd_pos(ix)) & overall_idx)), 'color', color, 'linestyle', '-');
            hold on
        end
    end
    hold off
    grid on
    xlim([-1 9]);
    ylim([-1.1 1.1] * max(abs(D(:))) + [0 max(shift(:))]);
    
    line(get(gca, 'xlim'), [0;0], 'linestyle', '--', 'color', 'k');
    line([0;0], get(gca, 'ylim'), 'linestyle', '-', 'color', 'k')
    
    set(gca, 'xtick', -1:9);
    if spread <= 0
       set(gca, 'ytick', -20:20, 'yticklabel', -20:20);
    else
       str = '';
       values = [];
       if numel(unique(data.ILD(ild_idx))) > 1
           str = [str ' %1.0fdB'];
           values = cat(2, values, data.ILD(overall_idx));
       end
       if numel(unique(data.ITD(itd_idx))) > 1
           str = [str ' %1.0f�s'];
           values = cat(2, values, data.ITD(overall_idx)/1e-6);
       end
       if numel(unique(data.Level(level_idx))) > 1
           str = [str ' %1.0fdB'];
           values = cat(2, values, data.Level(overall_idx));
       end
       labels = {};
       for k = 1:length(values)
           labels{k,1} = sprintf(str, values(k, :));
       end
       set(gca, 'ytick', unique(shift(:)), 'yticklabel', labels);
    end
    
    

    
    
    
