classdef feature_marker < handle

    properties (Access = public)
        
        dc_removal_time = 6
        time_limits = [0 9]
        noise_threshold = 3
        
    end
    
    properties (GetAccess = public)
        
        extremum_type = 'max'
        marker_type = 'x'
        is_selected = false
        is_enabled = false
        
    end
    
    properties (Access = private)
        
        time
        data
        
        latencies
        amplitudes
        
        idx
        
    end
    
    properties (Access = private)
        
        parent_handle
        marker_handle
        line_handle 
        
    end
    
    methods (Access = public)
        
        function this = feature_marker(time, data, parent, extremum_type, marker_type)
            
            this.time = time;
            this.data = data;
            
            if (ishandle(parent))
                this.parent_handle = parent;
            else
                warning('graphics parent handle not valid');
                this.parent_handle = [];
            end
            
            if ~ismember(extremum_type, {'min','max','ext'})
                error('extremum type not valid (use min/max/ext)');
            end
            this.extremum_type = extremum_type;
            
            this.marker_type = marker_type;

            this.is_enabled = true;
            
            this.get_extrema;
            this.draw_feature;
           
        end
        
        function delete(this)
            if ishandle(this.marker_handle)
                delete(this.marker_handle);
            end
            if ishandle(this.line_handle)
                delete(this.line_handle);
            end
        end
        
    end
    
    methods (Access = private)
        
        function get_extrema(this)
            % running average DC removal window
            M = round(this.dc_removal_time/(this.time(2)-this.time(1)));

            % remove DC
            if  M < size(this.data,1)/3
                baseline   = filtfilt(rectwin(M)/M,1,this.data);
            else
                baseline   = mean(this.data);
            end
            
            % discrete derivative
            D = diff(sign(diff(this.data - baseline)));
            switch this.extremum_type
                case 'ext'
                    pos = find(D~=0)+1;
                case 'max'
                    pos = find(D<0)+1;
                case 'min'
                    pos = find(D>0)+1;
            end
            
            % clean up candidate positions
            pos(this.time(pos) < this.time_limits(1) | ...
                this.time(pos) > this.time_limits(2)) = [];
            
            % skip noise extrema
            s  = std(this.data(this.time<0));
            this.idx = find(...
                abs(this.data(pos)) > this.noise_threshold * std(this.data(this.time<0)),...
                          1,'first');
            
            this.latencies = this.time(pos);
            this.amplitudes = this.data(pos);
        end
        
        function valid_flag = is_valid(this)
            valid_flag =    ~isempty(this.latencies) ...
                         && ~isempty(this.amplitudes) ...
                         && ~isempty(this.idx) ...
                         && this.idx > 0 ...
                         && this.idx <= length(this.latencies) ...
                         && this.idx <= length(this.amplitudes);
        end
        
    end
    
    methods (Access = public)
        
        function draw_feature(this)
            if is_valid(this) && this.is_enabled
                if ishandle(this.marker_handle)
                    delete(this.marker_handle)
                end
                this.marker_handle = line(this.latencies(this.idx), ...
                                          this.amplitudes(this.idx), ...
                                          'marker','v', ...
                                          'color','r', ...
                                          'linewidth',1, ...
                                          'linestyle','none', ...
                                          'markersize',8);
                if ishandle(this.line_handle)
                    delete(this.line_handle)
                end
                this.line_handle = line([1;1]*this.latencies(this.idx), ...
                                        [-3;3]*max(abs(this.data)), ...
                                          'marker','none', ...
                                          'color',[0.4 0.4 0.4], ...
                                          'linewidth',1, ...
                                          'linestyle','-');
            end
        end
        
        function move_feature(this)
            if ishandle(this.marker_handle)
                set(this.marker_handle, ...
                    'xdata', this.latencies(this.idx), ...
                    'ydata', this.amplitudes(this.idx));
            end
            if ishandle(this.line_handle)
                set(this.line_handle, ...
                    'xdata', [1;1]*this.latencies(this.idx), ...
                    'ydata', [-3;3]*max(abs(this.data)));
            end
        end
        
        function select_feature(this)
            if this.is_enabled
                this.is_selected = true;
                if ishandle(this.marker_handle)
                    set(this.marker_handle, 'linewidth', 3);
                end
            end
        end
        
        function unselect_feature(this)
            this.is_selected = false;
            if ishandle(this.marker_handle)
                set(this.marker_handle, 'linewidth', 1);
            end
        end

        function enable_feature(this)
            this.is_enabled = true;
            if ishandle(this.marker_handle)
                set(this.marker_handle, 'visible', 'on');
            end
            if ishandle(this.line_handle)
                set(this.line_handle, 'visible', 'on');
            end
        end
        
        function disable_feature(this)
            this.is_enabled = false;
            if ishandle(this.marker_handle)
                set(this.marker_handle, 'visible', 'off');
            end
            if ishandle(this.line_handle)
                set(this.line_handle, 'visible', 'off');
            end
        end
        
        
        function distance = get_distance(this, x, y)
            if ishandle(this.marker_handle)
                xdata = get(this.marker_handle, 'xdata');
                ydata = get(this.marker_handle, 'ydata');
            else
                xdata = Inf;
                ydata = Inf;
            end
            distance = (x-xdata).^2 + (y-ydata).^2;
        end
        
        function previous_feature(this)
            if this.idx > 1
                this.idx = this.idx - 1;
                move_feature(this)
            end
        end
        
        function next_feature(this)
            if this.idx < min(length(this.latencies), length(this.amplitudes))
                this.idx = this.idx + 1;
                move_feature(this)
            end
        end
        
        function set_earlier_than(this, time)
            pos = find(this.latencies < time, 1, 'last');
            if ~isempty(pos)
                this.idx = pos;
                move_feature(this);
            end
        end
        
        function set_later_than(this, time)
            pos = find(this.latencies > time, 1, 'first');
            if ~isempty(pos)
                this.idx = pos;
                move_feature(this);
            end
        end
        
    end

end
